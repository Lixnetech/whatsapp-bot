<?php

namespace App\Helpers;

use App\Models\Contact;

class Helper
{
    public static function kenyan($phone){
        return preg_replace('/^0/','254',$phone);
    }

    public static function formatPhone($phone){
       return  str_replace('@c.us','',$phone);
    }

    public static function contactName($number){
    $contact = Contact::where('number', $number)->first();

    if ($contact && $contact->first_name != null) {
        return $contact->first_name.' '.$contact->last_name;
    }

    return $number;
}
}
