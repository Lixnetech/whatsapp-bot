"use strict";
var KTAccountSettingsProfileDetails = (function () {
    var e, t;
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    return {
        init: function () {
            (e = document.getElementById("kt_account_profile_details_form")),
                e.querySelector("#kt_account_profile_details_submit"),
                (t = FormValidation.formValidation(e, {
                    fields: {
                        first_name: { validators: { notEmpty: { message: "First name is required" } } },
                        last_name: { validators: { notEmpty: { message: "Last name is required" } } },
                        company: { validators: { notEmpty: { message: "Company name is required" } } },
                        phone: { validators: { notEmpty: { message: "Contact phone number is required" } } },
                        address: { validators: { notEmpty: { message: "Your business address is required" } } },
                        pin: { validators: { notEmpty: { message: "Your KRA Pin is required" } } },
                        country: { validators: { notEmpty: { message: "Please select a country" } } },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }),
                    },
                })),
                e.querySelector("#kt_account_profile_details_submit").addEventListener("click", function (e) {
                    e.preventDefault(),
                        t.validate().then(function (t) {
                            "Valid" == t
                                ?
                                $.ajax({
                                    url: "update-information",
                                    method: 'post',
                                    data: $('#kt_account_profile_details_form').serialize(),
                                    success: function () {
                                        swal.fire({
                                            text: "Details Updated Successfully!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: { confirmButton: "btn font-weight-bold btn-light-primary" },
                                        }),
                                            location.reload()
                                    },error: function () {
                                        swal.fire({
                                            text: "Sorry, looks like there are some errors detected, please try again.",
                                            icon: "error",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Ok, got it!",
                                            customClass: { confirmButton: "btn font-weight-bold btn-light-primary" },
                                        })
                                    }
                                })
                                : swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: { confirmButton: "btn font-weight-bold btn-light-primary" },
                                });
                        });
                })
                var imgInputEl = e.querySelector('#kt_avatar_image_input');
                var imageInput = KTImageInput.getInstance(imgInputEl);
                    imageInput.on("kt.imageinput.changed",function (){
                    console.log('changed');
                    var files = imageInput.getInputElement();
                    var img = files.files
                    console.log(img.length)
                    if(img.length > 0){
                        var fd = new FormData();
                        //append data
                        fd.append('_token', CSRF_TOKEN)
                        fd.append('avatar',img[0]);
                        console.log(fd)

                        $.ajax({
                            url: 'update-avatar',
                            method: 'post',
                            data: fd,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            success: function (resp) {
                                location.reload()
                                if(resp.status === 'success'){
                                    console.log(resp.message)
                                }else{
                                    console.log(resp.message)
                                }
                            },
                            error: function (resp) {
                                console.log(resp)
                            }
                        })
                    }

                })
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTAccountSettingsProfileDetails.init();
});
