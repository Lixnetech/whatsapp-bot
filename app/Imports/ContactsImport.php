<?php

namespace App\Imports;

use App\Models\Contact;
use App\Models\ContactGroup;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;


class ContactsImport implements ToModel,WithHeadingRow, WithChunkReading,WithUpserts
{
    protected $client_id;
    protected $group_id;

    public function __construct($client,$group)
    {
        $this->client_id = $client;
        $this->group_id = $group;
    }

    public function uniqueBy()
    {
        return 'number';
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Contact([
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'group_id' => $this->group_id,
            'client_id' => $this->client_id,
            'number'=> $row['number'],
        ]);
    }


    public function chunkSize(): int
    {
        return 1000;
    }
}
