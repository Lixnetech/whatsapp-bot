@extends('layouts.index')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
        <div class="toolbar" id="kt_toolbar">
            <!--begin::Container-->
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <!--begin::Title-->
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Dashboard
                        <!--begin::Separator-->
                        <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
                        <!--end::Separator-->
                    </h1>
                    <!--end::Title-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Row-->

        <!--end::Row-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-xxl">
                <!--begin::Row-->
                <div class="row g-5 g-xl-8 mb-4">
                    <div class="col-xl-3">
                        <!--begin::Statistics Widget 5-->
                        <a href="#" class="card bg-body hoverable card-xl-stretch mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen032.svg-->
                                <span class="svg-icon svg-icon-primary svg-icon-3x ms-n1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect x="8" y="9" width="3" height="10" rx="1.5" fill="black" />
                                    <rect opacity="0.5" x="13" y="5" width="3" height="14" rx="1.5" fill="black" />
                                    <rect x="18" y="11" width="3" height="8" rx="1.5" fill="black" />
                                    <rect x="3" y="13" width="3" height="6" rx="1.5" fill="black" />
                                </svg>
                            </span>
                                <!--end::Svg Icon-->
                                <div class="text-gray-900 fw-bolder fs-2 mb-2 mt-5">{{$contacts->count()}}</div>
                                <div class="fw-bold text-gray-400">Contacts</div>
                            </div>
                            <!--end::Body-->
                        </a>
                        <!--end::Statistics Widget 5-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Statistics Widget 5-->
                        <a href="#" class="card bg-warning hoverable card-xl-stretch mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Svg Icon | path: icons/duotune/finance/fin006.svg-->
                                <span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="black" />
                                    <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="black" />
                                </svg>
                            </span>
                                <!--end::Svg Icon-->
                                <div class="text-white fw-bolder fs-2 mb-2 mt-5">{{$messages->count()}}</div>
                                <div class="fw-bold text-white">Messages Sent</div>
                            </div>
                            <!--end::Body-->
                        </a>
                        <!--end::Statistics Widget 5-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Statistics Widget 5-->
                        <a href="#" class="card bg-success hoverable card-xl-stretch mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Svg Icon | path: icons/duotune/finance/fin006.svg-->
                                <span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M10.9607 12.9128H18.8607C19.4607 12.9128 19.9607 13.4128 19.8607 14.0128C19.2607 19.0128 14.4607 22.7128 9.26068 21.7128C5.66068 21.0128 2.86071 18.2128 2.16071 14.6128C1.16071 9.31284 4.96069 4.61281 9.86069 4.01281C10.4607 3.91281 10.9607 4.41281 10.9607 5.01281V12.9128Z" fill="black"></path>
														<path d="M12.9607 10.9128V3.01281C12.9607 2.41281 13.4607 1.91281 14.0607 2.01281C16.0607 2.21281 17.8607 3.11284 19.2607 4.61284C20.6607 6.01284 21.5607 7.91285 21.8607 9.81285C21.9607 10.4129 21.4607 10.9128 20.8607 10.9128H12.9607Z" fill="black"></path>
													</svg>
                                </span>
                                <!--end::Svg Icon-->
                                <div class="text-white fw-bolder fs-2 mb-2 mt-5">{{$groups->count()}}</div>
                                <div class="fw-bold text-white">Contact Groups</div>
                            </div>
                            <!--end::Body-->
                        </a>
                        <!--end::Statistics Widget 5-->
                    </div>
                </div>
                <div class="g-5 g-xl-8">
                    <!--begin::Col-->
                    <div class="col-lg-12">
                        <!--begin::Mixed Widget 5-->
                        <div class="card card-xxl-stretch mb-xl-8">
                            <!--begin::Beader-->
                            <div class="card-header border-0 py-5">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bolder fs-3 mb-1">Outgoing Messages This Month</span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body d-flex flex-column">
                                <!--begin::Chart-->
                                <div class="mixed-widget-chart card-rounded-top" data-kt-chart-color="success" style="height: 150px"></div>
                                <!--end::Chart-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Mixed Widget 5-->
                    </div>
                    <!--end::Col-->
                </div>

                <div class="g-5 g-xl-8">
                    <!--begin::Col-->
                    <div class="col-lg-12">
                        <!--begin::Mixed Widget 5-->
                        <div class="card card-xxl-stretch mb-xl-8">
                            <!--begin::Beader-->
                            <div class="card-header border-0 py-5">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bolder fs-3 mb-1">Incoming Messages This Month</span>

                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body d-flex flex-column">
                                <!--begin::Chart-->
                                <div class="inbound-chart card-rounded-top" data-kt-chart-color="success" style="height: 150px"></div>
                                <!--end::Chart-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Mixed Widget 5-->
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Row-->

            <!--end::Modals-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
@endsection
@push('scripts')
    <script>

         $(window).on('load',function (){
             let smsOutBoundOptions = {
                 chart: {
                     height: 270,
                     toolbar: {show: false},
                     type: 'line',
                     dropShadow: {
                         enabled: true,
                         top: 20,
                         left: 2,
                         blur: 6,
                         opacity: 0.20
                     }
                 },
                 stroke: {
                     curve: 'smooth',
                     width: 4
                 },
                 grid: {
                     borderColor: "#e7eef7"
                 },
                 legend: {
                     show: false
                 },
                 fill: {
                     type: 'gradient',
                     gradient: {
                         shade:'dark',
                         inverseColors: false,
                         gradientToColors: '#7367F0',
                         shadeIntensity: 1,
                         type: 'horizontal',
                         opacityFrom: 1,
                         opacityTo: 1,
                         stops: [0,100,100,100]
                     }
                 },
                 markers: {
                     size: 0,
                     hover: {
                         size: 5
                     }
                 },
                 xaxis: {
                     labels: {
                         style: {
                             colors: '#b9c3cd'
                         }
                     },
                     axisTicks: {
                         show: false
                     },
                     categories: {!! $outgoing->xAxis() !!},
                     axisBorder: {
                         show: false
                     },
                     tickPlacement: 'on',
                     type: 'string'
                 },
                 yaxis: {
                     tickAmount: 5,
                     labels: {
                         style: {
                             color: '#b9c3cd'
                         },
                         formatter: function (val) {
                             return val > 999 ? (val / 1000).toFixed(1) + 'k' : val
                         }
                     }
                 },
                 tooltip: {
                     x: {show:false}
                 },
                 series: {!! $outgoing->dataSet() !!}

             }
             let smsOutbound = new ApexCharts(
                 document.querySelector(".mixed-widget-chart"),
                 smsOutBoundOptions
             )
             smsOutbound.render();

             let smsInBoundOptions = {
                 chart: {
                     height: 270,
                     toolbar: {show: false},
                     type: 'line',
                     dropShadow: {
                         enabled: true,
                         top: 20,
                         left: 2,
                         blur: 6,
                         opacity: 0.20
                     }
                 },
                 stroke: {
                     curve: 'smooth',
                     width: 4
                 },
                 grid: {
                     borderColor: "#e7eef7"
                 },
                 legend: {
                     show: false
                 },
                 fill: {
                     type: 'gradient',
                     gradient: {
                         shade:'dark',
                         inverseColors: false,
                         gradientToColors: '#7367F0',
                         shadeIntensity: 1,
                         type: 'horizontal',
                         opacityFrom: 1,
                         opacityTo: 1,
                         stops: [0,100,100,100]
                     }
                 },
                 markers: {
                     size: 0,
                     hover: {
                         size: 5
                     }
                 },
                 xaxis: {
                     labels: {
                         style: {
                             colors: '#b9c3cd'
                         }
                     },
                     axisTicks: {
                         show: false
                     },
                     categories: {!! $inbound->xAxis() !!},
                     axisBorder: {
                         show: false
                     },
                     tickPlacement: 'on',
                     type: 'string'
                 },
                 yaxis: {
                     tickAmount: 5,
                     labels: {
                         style: {
                             color: '#b9c3cd'
                         },
                         formatter: function (val) {
                             return val > 999 ? (val / 1000).toFixed(1) + 'k' : val
                         }
                     }
                 },
                 tooltip: {
                     x: {show:false}
                 },
                 series: {!! $inbound->dataSet() !!}

             }
             let smsInbound = new ApexCharts(
                 document.querySelector(".inbound-chart"),
                 smsInBoundOptions
             )
             smsInbound.render();
         })
    </script>
    <script>
        $(document).ready(function () {
            $('#kt_table_all_messages').delegate('#view-msg','click',function (e){
                let id = $(this).attr('rel')
                $.ajax({
                    url: "{{url('/reports')}}"+'/' + id + '/view',
                    type: 'POST',
                    data:{
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        console.log(data)
                        Swal.fire({
                            html: `<div class="table-responsive">
<table class="table table-striped text-left">

        <tbody>
            <tr>
                <td>{{ "From" }}</td>
                <td>` + data.data.from + `</td>
            </tr>
            <tr>
                <td>{{ "To" }}</td>
                <td>` + data.data.to + `</td>
            </tr>
            <tr>
                <td>{{ "Message" }}</td>
                <td>` + data.data.body + `</td>
            </tr>
            <tr>
                <td>{{ "Type" }}</td>
                <td>` + data.data.media_type + `</td>
            </tr>
            <tr>
                <td>{{ "Status"}}</td>
                <td>` + "Delivered" + `</td>
            </tr>
            <tr>
                <td>{{ "Cost" }}</td>
                <td>` + "Kes. 1" + `</td>
            </tr>

</tbody>
</table>
</div>`})},
                    error: function (reject) {
                        if(reject.status === 422){
                            let errors = reject.responseJSON.errors;
                            $.each(errors,function (key,val) {
                                toastr.warning(value[0],"Attention",{
                                    positionClass: 'toast-top-right',
                                    containerId: 'toast-top-right',
                                    progressBar: true,
                                    closeButton: true,
                                    newestOnTop: true
                                })
                            })
                        }
                    }
                })

            })
        })
    </script>
@endpush
