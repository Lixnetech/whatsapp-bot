<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatMessageController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\WhatsAppController;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Auth::routes(['register'=>false]);
Route::get('logout',[LoginController::class,'logout'])->name('exit');

Route::group(['prefix'=>'admin' , 'as' => 'admin.', 'middleware' => ['auth','admin']],
function(){
    Route::get('/dashboard',[DashboardController::class,'index']);
});

Route::group(['prefix' => 'client', 'as' => 'client.', 'middleware' => ['auth']], function ()
	{
		Route::get('/dashboard', [ClientController::class,'index'])->name('ClientDashboard');
        Route::get('settings/',[ClientController::class,'settings'])->name('settings');
        Route::post('/update-password',[ClientController::class,'changePassword'])->name('update.password');
        Route::post('update-information',[ClientController::class,'update'])->name('update.information');
        Route::post('update_email',[ClientController::class,'changeEmail'])->name('update.email');
        Route::post('update-avatar',[ClientController::class,'updateAvatar'])->name('update.avatar');
        Route::get('avatar',[ClientController::class,'avatar'])->name('avatar');

});

Route::match(['get','post'],'import-contacts',[contactController::class,'import_contacts']);

Route::prefix('whatsapp')->name('whatsapp.')->group(function () {
    Route::get('/quick-send', [WhatsappController::class,'send_text'])->name('quick_send');
    Route::post('/bulk-send', [WhatsappController::class,'send_text'])->name('bulk');
    Route::post('sendmessage',[ChatMessageController::class,'sendMessage'])->name('message');
    Route::get('chat',[ChatMessageController::class,'fetch_messages'])->name('chat');
    Route::get('template_message',[ChatMessageController::class,'template_message'])->name('template_message');
    Route::get('media-send',[ChatMessageController::class,'media'])->name('media');
    Route::post('media-send',[ChatMessageController::class,'sendMediaMessage'])->name('media.send');
    Route::post('upload_file',[ChatMessageController::class,'uploadFile'])->name('upload.file');
    Route::get('campaign',[ChatMessageController::class,'campaignBuilder'])->name('campaign');
    Route::post('save_campaign',[ChatMessageController::class,'storeCampaign'])->name('save.campaign');
});

Route::prefix('template')->name('templates.')->group(function () {
    Route::get('/list', [TemplateController::class,'index'])->name('list');
    Route::get('/new', [TemplateController::class,'create'])->name('create');
    Route::post('save',[TemplateController::class,'save'])->name('save');
});



Route::prefix('reports')->name('reports.')->group(function () {
    Route::get('/all_messages', [ReportController::class,'index'])->name('messages');
    Route::get('/outbound_messages', [ReportController::class,'outbound_msgs'])->name('outbound');
    Route::get('/inbound_messages', [ReportController::class,'inbound_msgs'])->name('inbound');
    Route::post('/{id}/view', [ReportController::class,'view_message']);
});

Route::prefix('contacts')->name('contacts.')->group(function () {
    Route::get('/groups',[contactController::class,'show_groups'])->name('group.list');
    Route::get('/create_group',[contactController::class,'create'])->name('create');
    Route::post('/create_group', [contactController::class,'create_contact_group'])->name('create.group');
    Route::get('/view_group/{contact}', [contactController::class,'show'])->name('view');
    Route::get('show_contacts',[contactController::class,'get_contacts'])->name('list');
    Route::get('contact/{id}',[contactController::class,'create_contact'])->name('create.contact');
    Route::post('contact',[contactController::class,'create_contact'])->name('create.contact');
    Route::post('single_contact',[contactController::class,'single_contact'])->name('single.contact');
    Route::match(['get','post'],'{contact}/import-contacts',[contactController::class,'import_contacts'])->name('import');
    Route::get('{contact}/import', [contactController::class,'import'])->name('group.import');
    Route::post('{contact}/store_import', [contactController::class,'store_import'])->name('save.import');
});


Route::get('wap-api',[WhatsAppController::class,'auth']);
Route::get('chat-box',[WhatsAppController::class,'authCheck']); //call on open app\


Route::get('sample_file',function(){
    return response()->download(storage_path('app/contacts.csv'));
})->name('sample.file');

Route::post('/messages',[ChatMessageController::class,'save_message']);

