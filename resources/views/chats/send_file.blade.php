@extends('layouts.index')
@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
                     data-bs-target="#kt_account_profile_details" aria-expanded="true"
                     aria-controls="kt_account_profile_details">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Send Messages</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="kt_account_profile_details" class="collapse show">
                    <!--begin::Form-->
                    <form id="upload_form" class="form" method="POST" action="{{route('whatsapp.media.send')}}">@csrf
                    <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                        <!--begin::Input group-->
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">
                                    <span class="required">Send To Group</span>
                                    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
                                </label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <select name="group_id" aria-label="Select a Group" data-control="select2"
                                            data-placeholder="Select"
                                            class="form-select form-select-solid form-select-lg fw-bold">
                                        <option value="">Select a Group...</option>
                                        <option  value="all">All</option>
                                        @foreach($groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->

                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Message</label>
                                <div class="col-lg-8 fv-row">
                                    <!--begin::Form-->
                                        <div class="fv-row">
                                            <!--begin::Dropzone-->
                                            <div class="dropzone" id="kt_dropzonejs_example_1">
                                                <!--begin::Message-->
                                                <div class="dz-message needsclick">
                                                    <!--begin::Icon-->
                                                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                                    <!--end::Icon-->

                                                    <!--begin::Info-->
                                                    <div class="ms-4">
                                                        <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
{{--                                                        <span class="fs-7 fw-bold text-gray-400">Upload up to 10 files</span>--}}
                                                    </div>
                                                    <!--end::Info-->
                                                </div>
                                            </div>
                                            <!--end::Dropzone-->
                                        </div>
                                        <!--end::Input group-->
                                    <!--end::Form-->
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Caption</label>
                                <div class="col-lg-8 fv-row">
                                    <input type="text" name="caption"  class="form-control  form-control-solid" placeholder="Enter Caption">
                                </div>
                            </div>
                            <input hidden id="group_id" value="">
                            <input hidden id="caption" value="">
                        </div>
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
                            <button type="submit" class="btn btn-primary" id="">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var myDropzone = new Dropzone("#kt_dropzonejs_example_1", {
            url: "{{route('whatsapp.upload.file')}}",
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file", // The name that will be used to transfer the file
            acceptedFiles: ".jpeg,.jpg,.png,.pdf",
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            autoProcessQueue: false,
            init: function() {
                var dz = this;
                $('#upload_form').submit(function(e){
                    e.preventDefault();
                    url = $('#upload_form').attr('action');
                    fd = $('#upload_form').serialize();
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: fd,
                        success: function (res){
                            if(res.status === "success"){
                                var caption = res.data.caption;
                                var group = res.data.group_id;
                                $('#caption').val(caption);
                                $('#group_id').val(group);
                                dz.processQueue()
                            }else{
                                console.log('error')
                            }
                        }
                    })
                })

                this.on('sending', function(file,xhr,formData){
                    let caption = document.getElementById('caption').value
                    let grpId = document.getElementById('group_id').value
                    formData.append('caption',caption);
                    formData.append('grpId',grpId);
                })

                this.on("success",function (file, response) {
                    $('#upload_form')[0].reset();
                    $('#kt_dropzonejs_example_1').empty()
                })
            }
        });
    </script>
@endpush
