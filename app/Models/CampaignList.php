<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CampaignList extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaigns::class);
    }

    public function contactList(): BelongsTo
    {
        return $this->belongsTo(ContactGroup::class);
    }
}
