<?php

namespace App\Http\Requests;

use App\Rules\Phone;
use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'first_name'   => ['required', 'string', 'max:255'],
                'last_name'   => ['required', 'string', 'max:255'],
                'contact_no'   => ['required', 'numeric', new Phone($this->contact_no)],
                'website' => ['nullable', 'url'],
                'address' => ['required', 'string'],
                'pin' => ['required','string'],
                'country' => ['required','string']
        ];
    }
}
