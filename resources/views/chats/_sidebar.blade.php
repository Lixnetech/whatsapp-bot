<!--begin::Contacts-->
<div class="card card-flush">
    <!--begin::Card header-->
    <div class="card-header pt-7" id="kt_chat_contacts_header">
        <!--begin::Form-->
        <form class="w-100 position-relative" autocomplete="off">
            <!--begin::Icon-->
            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
            <span class="svg-icon svg-icon-2 svg-icon-lg-1 svg-icon-gray-500 position-absolute top-50 ms-5 translate-middle-y">
                                    <i class="bi bi-search"></i>
                                </span>
            <!--end::Svg Icon-->
            <!--end::Icon-->
            <!--begin::Input-->
            <input type="text" class="form-control form-control-solid px-15" name="search" value="" placeholder="Search by username or phone..." />
            <!--end::Input-->
        </form>
        <!--end::Form-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-5" id="kt_chat_contacts_body">
        <!--begin::List-->
        <div class="scroll-y me-n5 pe-5 h-200px h-lg-auto" data-kt-scroll="true"
             data-kt-scroll-activate="{default: false, lg: true}"
             data-kt-scroll-max-height="auto"
             data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_contacts_header"
             data-kt-scroll-wrappers="#kt_content, #kt_chat_contacts_body"
             data-kt-scroll-offset="0px">
            <!--begin::User-->
            @foreach($messages as $message)
            <div class="d-flex flex-stack py-4 chat-list">
                <!--begin::Details-->
                <div class="d-flex align-items-center">
{{--                    <!--begin::Avatar-->--}}
{{--                    <div class="symbol symbol-45px symbol-circle">--}}
{{--                        <span class="symbol-label bg-light-danger text-danger fs-6 fw-bolder">D</span>--}}
{{--                        <div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n2 mt-n2"></div>--}}
{{--                    </div>--}}
{{--                    <!--end::Avatar-->--}}
                    <!--begin::Details-->
                    <div class="ms-5">
                        <span data-id="{{$message->from}}" class="fs-5 fw-bolder text-gray-900 text-hover-primary mb-2">{{App\Helpers\Helper::contactName($message->from)}}</span>
                        <div class="fw-bold text-muted">{{$message->body}}</div>
                    </div>
                    <!--end::Details-->
                </div>
                <!--end::Details-->
                <!--begin::Last seen-->
                <div class="d-flex flex-column align-items-end ms-2">
                    <span class="text-muted fs-7 mb-1">{{ReadableTime($message->created_at)}}</span>
{{--                    <span class="badge badge-sm badge-circle badge-light-warning">9</span>--}}
                </div>
                <!--end::Last seen-->
            </div>
            @endforeach
            <!--end::User-->
            <!--begin::Separator-->
            <div class="separator separator-dashed d-none"></div>
            <!--end::Separator-->

        </div>
        <!--end::List-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Contacts-->
