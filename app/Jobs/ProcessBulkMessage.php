<?php

namespace App\Jobs;

use App\Models\Contact;
use GuzzleHttp\Client;
use http\Exception\RuntimeException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\ThrottlesExceptions;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProcessBulkMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $message;
    private $phone;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request,$phone)
    {
        $this->message = $request->get('message');
        $this->phone = $phone;
    }

    public function middleware()
    {
        return [
             (new ThrottlesExceptions(3,10))->backoff(5)
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = [
            "message" => $this->message
        ];
        $url = 'https://396b-41-90-17-56.ngrok.io/chat/sendmessage/' . 254791876624;

       $response = Http::post($url,$params);

       if($response->failed()){
           throw new RuntimeException('Failed to Connect');
       }
    }
}
