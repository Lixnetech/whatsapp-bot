<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CampaignRecipients extends Model
{
    use HasFactory;

    protected $table = 'campaign_recipients';

    protected $guarded = [];

    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaigns::class);
    }
}
