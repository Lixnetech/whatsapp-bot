@extends('layouts.index')
@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
                     data-bs-target="#kt_account_profile_details" aria-expanded="true"
                     aria-controls="kt_account_profile_details">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Send Messages</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="kt_account_profile_details" class="collapse show">
                    <!--begin::Form-->
                    <form  class="form" method="POST" action="{{route('whatsapp.save.campaign')}}">@csrf
                        <div class="card-body border-top p-9">
                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">
                                    <span class="required">Campaign Name</span>
                                </label>
                                <div class="col-lg-8 fv-row">
                                    <input name="campaign_name"  class="form-control" placeholder="Required" autofocus>
                                </div>
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">
                                    <span class="required">Contact Groups</span>
                                 </label>
                                <div class="col-lg-8 fv-row">
                                    <select name="contact_groups[]" aria-label="Select Groups" data-control="select2"
                                            class="form-select form-select-solid" data-control="select2"
                                            data-allow-clear="true" multiple="multiple"
                                            class="form-select form-select-solid form-select-lg fw-bold">
                                        @foreach($groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}} ({{$group->contacts->count()}})</option>
                                        @endforeach
                                    </select>
                                    @error('contact_groups')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <!--end::Col-->
                            </div>
{{--                            <div class="row mb-6">--}}
{{--                                <!--begin::Label-->--}}
{{--                                <label class="col-lg-4 col-form-label fw-bold fs-6">--}}
{{--                                    <span class="required">Template</span>--}}
{{--                                    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Message Template"></i>--}}
{{--                                </label>--}}
{{--                                <!--end::Label-->--}}
{{--                                <!--begin::Col-->--}}
{{--                                <div class="col-lg-8 fv-row">--}}
{{--                                    <select  aria-label="Select a Template" id="sms_template" data-control="select2"--}}
{{--                                            class="form-select form-select-solid" data-control="select2"--}}
{{--                                            data-allow-clear="true"--}}
{{--                                            class="form-select form-select-solid form-select-lg fw-bold">--}}
{{--                                        @foreach($templates as $template)--}}
{{--                                            <option value="{{$template->id}}">{{$template->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <!--end::Col-->--}}
{{--                            </div>--}}
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">
                                    <span class="required">Available Tags</span>
                                    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Select Tags to Include in message"></i>
                                </label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <select id="available_tag" aria-label="Select a Tag" data-control="select2"
                                            class="form-select form-select-solid" data-control="select2"
                                            data-allow-clear="true"
                                            class="form-select form-select-solid form-select-lg fw-bold">
                                        <option value="phone">Phone</option>
                                        <option value="first_name">First Name</option>
                                        <option value="last_name">Last Name</option>
                                        <option value="email">Email</option>
                                        @if($template_tags)
                                            @foreach($template_tags as $tag)
                                                <option value="{{$tag->tag}}">{{$tag->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Message</label>
                                <div class="col-lg-8 fv-row">
                                    <textarea type="text" name="message" id="sch_message" rows="5" class="form-control  form-control-solid" required placeholder="">
                                    </textarea>
                                </div>
                                @error('message')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Schedule</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <input type="checkbox" class="schedule" name="schedule" value="true" {{ old('schedule') == true ? "checked" : null }}>
                                </div>
                                <!--end::Col-->
                            </div>
                            <div class="row  schedule_time">
                                <!--begin::Label-->
                                <div class="col-md-6 col-form-label fw-bold fs-6">
                                    <label>Schedule Date</label>
                                    <input type="text" class="form-control schedule_date" name="schedule_date"
                                           placeholder="YYYY-MM-DD">
                                </div>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-md-6 fv-row">
                                    <label>Time</label>
                                    <input type="text" id="time" class="form-control flatpickr-time schedule_time" name="schedule_time"
                                           placeholder="HH:MM">
                                </div>
                                <div class="col-lg-12 fv-row">
                                    <label>Timezone</label>
                                    <select class="form-control" id="timezone" name="timezone" data-control="select2"
                                            class="form-select form-select-solid" data-control="select2"
                                            data-allow-clear="true">
                                        @foreach(\App\Helpers\Tool::allTimeZones() as $timezone)
                                        <option value="{{$timezone['zone']}}" {{ Auth::user()->timezone == $timezone['zone'] ? 'selected': null }}> {{ $timezone['text'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-12 fv-row">
                                    <label>Frequency</label>
                                    <select class="form-control" id="frequency_cycle"
                                            name="frequency_cycle">
                                        <option value="onetime" {{old('frequency_cycle')}}> One Time</option>
                                        <option value="daily" {{old('frequency_cycle')}}> Daily </option>
                                        <option value="monthly" {{old('frequency_cycle')}}> Monthly </option>
                                        <option value="yearly" {{old('frequency_cycle')}}> Yearly</option>
                                        <option value="custom" {{old('frequency_cycle')}}> Custom</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
                            <button type="submit" class="btn btn-primary" id="">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('assets/plugins/custom/dom-rules/dom-rules.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.schedule_date').flatpickr({
                minDate: "today",
                dateFormat: 'Y-m-d',
                defaultDate: "{{date('Y-m-d')}}"
            });
            $('.flatpickr-time').flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: 'H:i',
                defaultDate: '{{date('H:i')}}'
            })

            let schedule = $('.schedule'),
                scheduleTime = $('.schedule_time');

            if(schedule.prop('checked') === true){
                scheduleTime.show()
            }else{
                scheduleTime.hide()
            }

            schedule.change(function () {
                scheduleTime.fadeToggle();
            })

            {{--$(".select2").select2({--}}
            {{--    dropdownAutoWidth: true,--}}
            {{--    width: '100%',--}}
            {{--    theme: "classic",--}}
            {{--    placeholder: "{{ "Choose Your Option" }}"--}}
            {{--});--}}

            let maxCharInitial = 160,
                messages = 1,
                $get_msg = $('#sch_message'),
                merge_state = $('#available_tag');

            merge_state.on('change',function (){
                const caretPos = $get_msg[0].selectionStart;
                const textAreatxt = $get_msg.val();
                let txtToAdd = this.value;
                if(txtToAdd){
                    txtToAdd = '{' + txtToAdd + '}';
                }
                $get_msg.val(textAreatxt.substring(0,caretPos) + txtToAdd + textAreatxt.substring(caretPos))
            })

            $('#sms_tmeplate').on('change',function (){
                let template_id = $(this).val();

                $get_msg.keyup(get_character);
            })

            function get_character(){
                let totalChar = $get_msg[0].value.length;
                let remainingChar = maxCharInitial;

                if (totalChar <= maxCharInitial) {
                    remainingChar = maxCharInitial - totalChar;
                    messages = 1;
                } else {
                    totalChar = totalChar - maxCharInitial;
                    messages = Math.ceil(totalChar / maxChar);
                    remainingChar = messages * maxChar - totalChar;
                    messages = messages + 1;
                }

                $remaining.text(remainingChar + " {!! "characters Remaining" !!}");
                $messages.text(messages + " {!! __('locale.labels.message') !!}" + '(s)');
            }

            $.createDomRules({
                parentSelector: 'body',
                scopeSelector: 'form',
                showTargets: function (rule, $controller, condition, $targets, $scope) {
                    $targets.fadeIn();
                },
                hideTargets: function (rule, $controller, condition, $targets, $scope) {
                    $targets.fadeOut();
                },

                rules: [
                    {
                        controller: '#frequency_cycle',
                        value: 'custom',
                        condition: '==',
                        targets: '.show-custom',
                    },
                    {
                        controller: '#frequency_cycle',
                        value: 'onetime',
                        condition: '!=',
                        targets: '.show-recurring',
                    },
                    {
                        controller: '.message_type',
                        value: 'mms',
                        condition: '==',
                        targets: '.send-mms',
                    }
                ]
            });
        })
    </script>
@endpush
