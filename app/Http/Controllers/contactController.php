<?php

namespace App\Http\Controllers;

use App\Helpers\Tool;
use App\Imports\ContactsImport;
use App\Jobs\ImportContacts;
use App\Models\Contact;
use App\Models\ContactGroup;
use App\Models\CsvData;
use App\Models\ImportJobHistory;
use Illuminate\Bus\Batch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Exception;

class contactController extends Controller
{
    public function get_contacts(){
        $contacts = Contact::all();
        return view('contacts.index',compact('contacts'));
    }

    public function import_contacts(ContactGroup $contact,Request $request){
       $client = Auth::user()->customer->id;
        if($request->isMethod('POST')){
            $request->validate([
                'import_file' => 'required|file|max:2048'
            ]);
            try{

                Excel::import(new ContactsImport($client,$contact->id),$request->file('import_file'));
                return redirect()->route('contacts.view',$contact->id)->with('flash_message','Success! All good');
            }catch(Exception $e){

            }
        }
        return view('contacts.import');
    }

    public function create_contact(Request $request)
    {
        if($request->isMethod('POST')){
            $data['name'] = $request->first_name .''. $request->last_name;
            $data['number'] = $request->number;
            $data['client_id'] = Auth::user()->customer->id;
            $data['group_id'] = $request->group_id;

            try{
                DB::beginTransaction();
                Contact::create($data);
                DB::commit();
                return redirect()->back()->with('flash_message','Contact Added Successfully');
            }catch (\Exception $e){
                DB::commit();
                return redirect()->back()->with('flash_message','Contact Already Exists');

            }

        }
        return view('contactGroups.contact');
    }

    public function single_contact(Request $request){
        $data['name'] = $request->name;
        $data['number'] = $request->number;
        $data['client_id'] = Auth::user()->customer->id;

        Contact::create($data);

        return response()->json(['success'=>'success','flash_message'=>'Contact Added Successfully']);
    }

    public function create()
    {
        return view('contactGroups.create');
    }

    public function create_contact_group(Request $request)
    {
        $customer_id = Auth::user()->customer->id;
        $name = $request->name;
        $request->validate([
            'name' => ['required', Rule::unique('contact_groups')->where(function ($query) use ($customer_id, $name) {
                return $query->where('client_id', $customer_id)->where('name', $name);
            })]

        ]);

        $group = new ContactGroup();
        $group->name = $request->name;
        $group->client_id = Auth::user()->customer->id;
        $group->save();

        return redirect()->back()->with([
            'status' => 'Success',
            'message' => 'Contact Group created Successfully'
        ]);
    }

    public function show_groups()
    {
        $client_id = Auth::user()->customer->id;
        $groups = ContactGroup::where('client_id',$client_id)->get();

        return view('contactGroups.index',compact('groups'));
    }

    public function show(ContactGroup $contact)
    {
        $contacts = Contact::where(['group_id'=>$contact->id])->cursor();
        return view('contactGroups.view',compact('contacts','contact'));
    }

    public function import(ContactGroup $contact){
        return view('contactGroups.import',compact('contact'));
    }

    public function store_import(ContactGroup $contact,Request $request)
    {
        if($request->file('import_file')->isValid()){
            $path = $request->file('import_file')->getRealPath();
            $data = array_map('str_getcsv', file($path));

            $csv_file = CsvData::create([
                'user_id' => Auth::user()->id,
                'ref_id' => $contact->id,
                'ref_type'=> 'contacts',
                'csv_filename' => $request->file('import_file')->getClientOriginalName(),
                'header' => $request->has('header'),
                'data' => json_encode($data)
            ]);

            $csv_data = array_slice($data, 0,2);

            return view('contacts.import_fields',compact('csv_data','contact','csv_file'));

        }
        return redirect()->route('contacts.view',$contact->id)->with('flash_message','Something Went Wrong');
    }

    public function importProcessData(ContactGroup $contact,Request $request){
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data,true);
        $db_fields = $request->fields;

        if(is_array($db_fields) && !in_array('phone',$db_fields)){
            return redirect()->route('')->with([
               'status' => 'error',
               'message' => 'Phone Number Column is Required'
            ]);
        }
        $collection = collect($csv_data)->skip($data->csv_header);

        $batch_list = [];
        Tool::resetMaxExecutionTime();
        $collection->chunk(5000)
            ->each(function($lines) use ($contact,$batch_list,$db_fields){
                $batch_list[] = new ImportContacts(Auth::user()->id, $contact->id, $lines, $db_fields);
            });

        try{
            $import_name = 'ImportContacts_'.date('Ymdhms');

            $import_job = ImportJobHistory::create([
                'name'      => $import_name,
                'import_id' => $contact->id,
                'type'      => 'import_contact',
                'status'    => 'processing',
                'options'   => json_encode(['status' => 'processing', 'message' => 'Import contacts are running']),
                'batch_id'  => null,
            ]);

            $batch = Bus::batch($batch_list)
                ->then(function (Batch $batch) use ($contact, $import_name, $import_job) {
                    $contact->update(['batch_id' => $batch->id]);
                    $import_job->update(['batch_id' => $batch->id]);
                })
                ->catch(function (Batch $batch, \Throwable $e) {
                    $import_history = ImportJobHistory::where('batch_id', $batch->id)->first();
                    if ($import_history) {
                        $import_history->status  = 'failed';
                        $import_history->options = json_encode(['status' => 'failed', 'message' => $e->getMessage()]);
                        $import_history->save();
                    }

                })
                ->finally(function (Batch $batch) use ($contact, $data) {
                    $import_history = ImportJobHistory::where('batch_id', $batch->id)->first();
                    if ($import_history) {
                        $import_history->status  = 'finished';
                        $import_history->options = json_encode(['status' => 'finished', 'message' => 'Import contacts was successfully imported.']);
                        $import_history->save();
                    }

                    $contact->updateCache('SubscribersCount');
                    $data->delete();
                    //send event notification remaining
                })
                ->name($import_name)
                ->dispatch();

            $contact->update(['batch_id' => $batch->id]);

            return redirect()->route('customer.contacts.show', $contact->uid)->with([
                'status'  => 'success',
                'message' => 'Contacts imported successfully in the background',
            ]);

        } catch (\Throwable $e) {
            return redirect()->route('customer.contacts.show', $contact->uid)->with([
                'status'  => 'error',
                'message' => $e->getMessage(),
            ]);
        }
    }
}
