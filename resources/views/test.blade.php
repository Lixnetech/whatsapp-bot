@extends('layouts.index')
@section('content')
   <div class="post d-flex flex-column-fluid" id="kt_post">
       <div id="kt_content_container " class="container-xxl">
           <div id="qr">
            {!! QrCode::size(250)->generate($data) !!}
           </div>
       </div>
    </div>
@endsection
@push('scripts')
    <script>
        function loadqr(){
            $('#qr').load('chat-box')
            console.log('reload')
        }
        //loadqr();
        setInterval(() => {
            loadqr()
        }, 30000);
    </script>
@endpush
