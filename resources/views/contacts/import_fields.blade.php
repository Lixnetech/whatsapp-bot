@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Card-->
            <div class="card">

                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        @foreach($csv_data as $row)
                        <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                            @foreach($row as $key => $value)
                                <th class="text-end min-w-100px">{{$value}}</th>
                            @endforeach
                        </tr>
                        @endforeach
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="text-gray-600 fw-bold">
                            <tr>
                                @foreach($csv_data[0] as $key => $value)
                                <td>
                                    <select class="form-select form-select-solid" name="fields[{{$key}}]" data-control="select2"
                                            data-placeholder="Select an option"
                                            data-allow-clear="true" multiple="multiple">
                                        @foreach(config('app.db_fields') as $db_key => $db_field)
                                            <option value="{{$db_key}}">{{$db_field}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                @endforeach
                            </tr>
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
@endsection
