@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Layout-->
            <div class="d-flex flex-column flex-lg-row">
                <!--begin::Sidebar-->
                <div class="flex-column flex-lg-row-auto w-100 w-lg-300px w-xl-400px mb-10 mb-lg-0">
                    @include('chats._sidebar')
                </div>
                <!--end::Sidebar-->
                <!--begin::Content-->
                <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                    <!--begin::Messenger-->
                    <div class="card user-chats" id="kt_chat_messenger">
                        <!--begin::Card header-->
                    {{--                        <div class="card-header" id="kt_chat_messenger_header">--}}
                    {{--                            <!--begin::Title-->--}}
                    {{--                            <div class="card-title">--}}
                    {{--                                <!--begin::User-->--}}
                    {{--                                <div class="d-flex justify-content-center flex-column me-3">--}}
                    {{--                                    <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 mb-2 lh-1">Davies</a>--}}
                    {{--                                    <!--begin::Info-->--}}
                    {{--                                    <div class="mb-0 lh-1">--}}
                    {{--                                        <span class="badge badge-success badge-circle w-10px h-10px me-1"></span>--}}
                    {{--                                        <span class="fs-7 fw-bold text-muted">Active</span>--}}
                    {{--                                    </div>--}}
                    {{--                                    <!--end::Info-->--}}
                    {{--                                </div>--}}
                    {{--                                <!--end::User-->--}}
                    {{--                            </div>--}}
                    {{--                            <!--end::Title-->--}}
                    {{--                            <!--begin::Card toolbar-->--}}
                    {{--                            <div class="card-toolbar">--}}
                    {{--                                <!--begin::Menu-->--}}
                    {{--                                <div class="me-n3">--}}
                    {{--                                    <button class="btn btn-sm btn-icon btn-active-light-primary"--}}
                    {{--                                            data-kt-menu-trigger="click"--}}
                    {{--                                            data-kt-menu-placement="bottom-end">--}}
                    {{--                                        <i class="bi bi-three-dots fs-2"></i>--}}
                    {{--                                    </button>--}}
                    {{--                                    <!--begin::Menu 3-->--}}
                    {{--                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">--}}
                    {{--                                        <!--begin::Heading-->--}}
                    {{--                                        <div class="menu-item px-3">--}}
                    {{--                                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Contacts</div>--}}
                    {{--                                        </div>--}}
                    {{--                                        <!--end::Heading-->--}}
                    {{--                                        <!--begin::Menu item-->--}}
                    {{--                                        <div class="menu-item px-3">--}}
                    {{--                                            <a href="#" class="menu-link px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_users_search">Add Contact</a>--}}
                    {{--                                        </div>--}}
                    {{--                                        <!--end::Menu item-->--}}
                    {{--                                        <!--begin::Menu item-->--}}
                    {{--                                        <div class="menu-item px-3">--}}
                    {{--                                            <a href="#" class="menu-link flex-stack px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_invite_friends">Invite Contacts--}}
                    {{--                                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a contact email to send an invitation"></i></a>--}}
                    {{--                                        </div>--}}
                    {{--                                        <!--end::Menu item-->--}}
                    {{--                                        <!--begin::Menu item-->--}}
                    {{--                                        <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">--}}
                    {{--                                            <a href="#" class="menu-link px-3">--}}
                    {{--                                                <span class="menu-title">Groups</span>--}}
                    {{--                                                <span class="menu-arrow"></span>--}}
                    {{--                                            </a>--}}
                    {{--                                            <!--begin::Menu sub-->--}}
                    {{--                                            <div class="menu-sub menu-sub-dropdown w-175px py-4">--}}
                    {{--                                                <!--begin::Menu item-->--}}
                    {{--                                                <div class="menu-item px-3">--}}
                    {{--                                                    <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="Coming soon">Create Group</a>--}}
                    {{--                                                </div>--}}
                    {{--                                                <!--end::Menu item-->--}}
                    {{--                                                <!--begin::Menu item-->--}}
                    {{--                                                <div class="menu-item px-3">--}}
                    {{--                                                    <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="Coming soon">Invite Members</a>--}}
                    {{--                                                </div>--}}
                    {{--                                                <!--end::Menu item-->--}}
                    {{--                                                <!--begin::Menu item-->--}}
                    {{--                                                <div class="menu-item px-3">--}}
                    {{--                                                    <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="Coming soon">Settings</a>--}}
                    {{--                                                </div>--}}
                    {{--                                                <!--end::Menu item-->--}}
                    {{--                                            </div>--}}
                    {{--                                            <!--end::Menu sub-->--}}
                    {{--                                        </div>--}}
                    {{--                                        <!--end::Menu item-->--}}
                    {{--                                        <!--begin::Menu item-->--}}
                    {{--                                        <div class="menu-item px-3 my-1">--}}
                    {{--                                            <a href="#" class="menu-link px-3" data-bs-toggle="tooltip" title="Coming soon">Settings</a>--}}
                    {{--                                        </div>--}}
                    {{--                                        <!--end::Menu item-->--}}
                    {{--                                    </div>--}}
                    {{--                                    <!--end::Menu 3-->--}}
                    {{--                                </div>--}}
                    {{--                                <!--end::Menu-->--}}
                    {{--                            </div>--}}
                    {{--                            <!--end::Card toolbar-->--}}
                    {{--                        </div>--}}
                    <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body chats" id="kt_chat_messenger_body">
                            <!--begin::Messages-->
                            <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto chat-history"
                                 data-kt-element="messages"
                                 data-kt-scroll="true"
                                 data-kt-scroll-activate="{default: false, lg: true}"
                                 data-kt-scroll-max-height="auto"
                                 data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer"
                                 data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body"
                                 data-kt-scroll-offset="-2px">
                                <!--begin::Message(template for out)-->
                                <div class="d-flex justify-content-end mb-10 d-none" data-kt-element="template-out">

                                </div>
                                <!--end::Message(template for out)-->
                                <!--begin::Message(template for in)-->
                                <div class="d-flex justify-content-start mb-10 d-none" data-kt-element="template-in">

                                </div>
                                <!--end::Message(template for in)-->
                            </div>
                            <!--end::Messages-->
                        </div>
                        <!--end::Card body-->
                        <!--begin::Card footer-->
                        <div class="card-footer pt-4" id="kt_chat_messenger_footer">
                            <form id="kt_chat" onsubmit="enter_chat();" action="javascript:void(0);" enctype="multipart/form-data">@csrf
                            <!--begin::Input-->
                                <textarea class="form-control form-control-flush mb-3" rows="1" name="message" id="txt_message"  placeholder="Type a message"></textarea>
                                <!--end::Input-->
                                <!--begin:Toolbar-->
                                <div class="d-flex flex-stack">
                                    <!--begin::Actions-->
                                    <div class="d-flex align-items-center me-2">
                                        <div class="image-input" data-kt-image-input="true" hidden>
                                            <!--begin::Image preview wrapper-->
                                            <div class="image-input-wrapper w-125px h-125px" style="background-image: url(/assets/media/avatars/150-5.jpg)"></div>
                                            <!--end::Image preview wrapper-->
                                            <!--begin::Inputs-->
                                            <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" name="image_remove" />
                                            <!--end::Inputs-->
                                        </div>
                                        <button class="btn btn-sm btn-icon btn-active-light-primary me-1"
                                                type="button"
                                                data-kt-image-input-action="upload"
                                                data-bs-toggle="tooltip"
                                                data-bs-dismiss="click"
                                        >
                                            <i class="bi bi-paperclip fs-3"></i>
                                        </button>
                                        <button class="btn btn-sm btn-icon btn-active-light-primary me-1" type="button" data-bs-toggle="tooltip" title="Coming soon">
                                            <i class="bi bi-upload fs-3"></i>
                                        </button>
                                    </div>
                                    <!--end::Actions-->
                                    <!--begin::Send-->
                                    <button class="btn btn-primary" type="button" onclick="enter_chat();">Send</button>
                                    <!--end::Send-->
                                </div>
                                <!--end::Toolbar-->
                            </form>

                        </div>
                        <!--end::Card footer-->
                    </div>
                    <!--end::Messenger-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Layout-->
            <!--begin::Modals-->
            <!--end::Modals-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
@endsection
@push('scripts')
    <script>
        $(function(){
            let chatHistory = $('.chat-history'),
                details,
                chatInput = $('#txt_message'),
                chatContainer = $('.user-chats');
            var socket = io.connect('http://localhost:4000');
            socket.on('connect',function () {
                socket.emit('user_connected')
            })
            socket.on('message',function (data){
                console.log(data)
                if(data.from){
                    window.location.reload()
                }
            })
            $('#send-msg').click(function (e) {
                e.preventDefault();
                var message = $('#message').val();
                if(message !== ''){
                    $.ajax({
                        type: "post",
                        url: '{{route('whatsapp.message')}}',
                        dataType: 'json',
                        data: $('#kt_chat').serialize(),
                        success: function (data) {
                            console.log(data)
                        }
                    })
                }
            })

            function convertImage(string){
                var image = new Image();
                image.src = document.getElementById('source').innerHTML;
                return document.body.appendChild(image);
            }

            function decode(string){
                var image = new Image()
                image.src = `data:image/png;base64,${string}`;
                return image;
            }


            $('.chat-list span').on('click',function (e){
                chatHistory.empty();
                chatContainer.animate({scrollTop: chatContainer[0].scrollHeight}, 400);
                let chat_id = $(this).data('id');
                $.ajax({
                    type: 'Get',
                    url: 'http://localhost:4000/chat/getchatbyid/'+ chat_id,
                    success: function (resp){

                        console.log(resp);
                        details = '<input type="hidden" value="' + chat_id + '" name="chat_id" class="chat_id">';
                        $.each(resp.message, function (i,message){
                            if(message.from === chat_id){
                                //console.log(chat_id)
                                details += '<div class="d-flex justify-content-start mb-10">' +
                                    '<div class="d-flex flex-column align-items-start">' +
                                    '<div class="d-flex align-items-center mb-2">'+
                                    <!--begin::Avatar-->
                                    '<div class="symbol symbol-35px symbol-circle">'+
                                    '<img alt="Pic" src="{{asset("./assets/media/avatars/blank.png")}}">'+
                                    '</div>'+
                                    '<div class="ms-3">'+
                                    '<a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">'+ message.from +'</a>'+
                                    '<span class="text-muted fs-7 mb-1">' + timeFormat(message.timestamp) +'</span>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" ' +
                                    'data-kt-element="message-text">'+ message.body +'</div>' +
                                    '</div>'+
                                    '</div>';
                            }
                            else{
                                details += '<div class="d-flex justify-content-end mb-10">'+
                                    <!--begin::Wrapper-->
                                    ' <div class="d-flex flex-column align-items-end">'+
                                    <!--begin::User-->
                                    '<div class="d-flex align-items-center mb-2">'+
                                    <!--begin::Details-->
                                    '<div class="me-3">'+
                                    '<span class="text-muted fs-7 mb-1">' + timeFormat(message.timestamp).toLocaleTimeString() +'</span>' +
                                    '<a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">'+  "You" +'</a>' +
                                    '</div>'+
                                    <!--end::Details-->
                                    <!--begin::Avatar-->
                                    '<div class="symbol symbol-35px symbol-circle">' +
                                    '<img alt="Pic" src="{{asset("assets/media/logos/isuzu.jpg")}}">' +
                                    '</div>' +
                                    <!--end::Avatar-->
                                    '</div>'+
                                    <!--end::User-->
                                    <!--begin::Text-->
                                    '<div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end" ' +
                                    'data-kt-element="message-text">'+
                                    message.body +'</div>'+
                                    <!--end::Text-->
                                    '</div>'+
                                    '</div>';
                            }
                        })
                        chatHistory.append(details);
                        chatContainer.animate({scrollTop: chatContainer[0].scrollHeight}, 400);
                    }
                })
            })


            chatInput.keypress(function (e){
                let message = $(this).html();
                if(e.which === 13 && !e.shiftKey){
                    chatInput.html("");
                    sendMessage(message);
                    return false;
                }
            })

            function sendMessage(message){
                let phone = $('.chat_id').val();
                let url = 'http://localhost:4000/chat/sendmessage/';

                appendMessageToSender(message);

                $.ajax({
                    url: url + phone,
                    type: 'POST',
                    data: message,
                    processData: false,
                    contentType: false,
                    dataType: 'JSON',
                    success: function (resp){
                        if(resp.status === "success"){
                            console.log('success')
                        }
                    }
                })
            }

            function appendMessageToSender(message){
                let text = '<div class="d-flex justify-content-end mb-10">'+
                    ' <div class="d-flex flex-column align-items-end">'+
                    '<div class="d-flex align-items-center mb-2">'+
                    '<div class="me-3">'+
                    '<span class="text-muted fs-7 mb-1">' +
                    getCurrentDateTime()
                    +'</span>' +
                    '<a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">'+
                    "You"
                    +'</a>' +
                    '</div>'+
                    '<div class="symbol symbol-35px symbol-circle">' +
                    '<img alt="Pic" src="{{asset("assets/media/logos/isuzu.jpg")}}">' +
                    '</div>' +
                    '</div>'+
                    '<div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">'+
                    message
                    +'</div>'+
                    '</div>'+
                    '</div>';
                chatHistory.append(text);
                $(".user-chats").scrollTop($(".user-chats > .chats").height());
            }

            {{--function enter_chat(source){--}}
            {{--    let message = $('#txt_message'),--}}
            {{--        phone = $('.chat_id').val(),--}}
            {{--        messageVal = message.val();--}}
            {{--    if(source.which === 13&& !source.shiftKey){--}}

            {{--    }--}}
            {{--    $.ajax({--}}
            {{--        url: 'http://localhost:4000/chat/sendmessage/'+ phone,--}}
            {{--        type: "post",--}}
            {{--        data: {--}}
            {{--            message: messageVal--}}
            {{--        },--}}
            {{--        success: function (resp){--}}
            {{--            if(resp.status === 'success'){--}}
            {{--                let msg = '<div class="d-flex justify-content-end mb-10">'+--}}
            {{--                    ' <div class="d-flex flex-column align-items-end">'+--}}
            {{--                    '<div class="d-flex align-items-center mb-2">'+--}}
            {{--                    '<div class="me-3">'+--}}
            {{--                    '<span class="text-muted fs-7 mb-1">' +--}}
            {{--                    'now'--}}
            {{--                    +'</span>' +--}}
            {{--                    '<a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">'+--}}
            {{--                    "You"--}}
            {{--                    +'</a>' +--}}
            {{--                    '</div>'+--}}
            {{--                    '<div class="symbol symbol-35px symbol-circle">' +--}}
            {{--                    '<img alt="Pic" src="{{asset("assets/media/logos/isuzu.jpg")}}">' +--}}
            {{--                    '</div>' +--}}
            {{--                    '</div>'+--}}
            {{--                    '<div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">'+--}}
            {{--                    messageVal--}}
            {{--                    +'</div>'+--}}
            {{--                    '</div>'+--}}
            {{--                    '</div>';--}}
            {{--            }--}}
            {{--            message.val("");--}}
            {{--            chatHistory.append(msg);--}}
            {{--            $(".user-chats").scrollTop($(".user-chats > .chats").height());--}}
            {{--        },--}}
            {{--        error: function (rej) {--}}
            {{--            if(rej.status === 422){--}}
            {{--                let errors = reject.responseJSON.errors;--}}
            {{--                console.log(errors)--}}
            {{--            }--}}
            {{--        }--}}
            {{--    })--}}
            {{--}--}}
        })
    </script>
@endpush
