@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
                     data-bs-target="#kt_account_profile_details" aria-expanded="true"
                     aria-controls="kt_account_profile_details">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Import Contacts</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="kt_account_profile_details" class="collapse show">
                    <!--begin::Form-->

                    <form action="{{route('contacts.import',$contact->id)}}" method="POST" class="form" enctype="multipart/form-data" >@csrf
                    <!--begin::Modal body-->
                        <div class=" pt-10 pb-15 px-lg-17">
                            <div class="form-group">
                                <p class="text-uppercase">Sample File</p>
                                <a href="{{route('sample.file')}}" class="btn btn-primary mb-10">
                                    <i class="feather icon-file-text"></i> Download Sample File
                                </a>

                            </div>
                            <!--begin::Input group-->
                            <div class="form-group">
                                <!--begin::Dropzone-->
                                <input type="file" class="btn btn-sm btn-primary me-2" name="import_file" placeholder="Attach files">
                                <!--end::Dropzone-->
                            </div>
                            <span class="form-text fs-6 text-muted">Max file size is 2MB per file.</span>
                            @error('import_file')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="card-footer d-flex justify-content-end py-6 px-9">
                                <button type="submit" class="btn btn-primary" id="">Upload Contacts</button>
                            </div>
                        </div>
                        <!--end::Modal body-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Basic info-->
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        const e = "#upload_dropzone",
            t = document.querySelector(e);
        var n = t.querySelector(".dropzone-item");
        n.id = "";
        var a = n.parentNode.innerHTML;
        n.parentNode.removeChild(n);
        var dropzone =  new Dropzone(e, {
            parallelUploads: 3,
            previewTemplate: a,
            maxFilesize: 5,
            autoProcessQueue: !1,
            autoQueue: !1,
            acceptedFiles: ".csv",
            previewsContainer: e + " .dropzone-items",
            clickable: e + " .dropzone-select",
        });
        dropzone.on("addedfile", function (n) {
            (n.previewElement.querySelector(e + " .dropzone-start").onclick = function () {
                const e = n.previewElement.querySelector(".progress-bar");
                e.style.opacity = "1";
                var t = 1,
                    a = setInterval(function () {
                        t >= 100 ? (dropzone.emit("success", n), dropzone.emit("complete", n),
                            clearInterval(a)) : (t++, (e.style.width = t + "%"));
                    }, 20);
            }),
                t.querySelectorAll(".dropzone-item").forEach((e) => {
                    e.style.display = "";
                }),
                (t.querySelector(".dropzone-upload").style.display = "inline-block"),
                (t.querySelector(".dropzone-remove-all").style.display = "inline-block");
        })
        dropzone.on("complete", function (e) {
            const n = t.querySelectorAll(".dz-complete");
            //upload with ajax
            setTimeout(function () {
                n.forEach((e) => {
                    (e.querySelector(".progress-bar").style.opacity = "0"), (e.querySelector(".progress").style.opacity = "0"), (e.querySelector(".dropzone-start").style.opacity = "0");
                });
            }, 300);

            dropzone.uploadFiles = function (files) {
                var self = this;
                for(var i= 0;  i <files.length; i++){
                    var file = files[i];

                }
            }
        })
        t.querySelector(".dropzone-upload").addEventListener("click", function () {
            dropzone.files.forEach((e) => {
                const t = e.previewElement.querySelector(".progress-bar");
                t.style.opacity = "1";
                var n = 1,
                    a = setInterval(function () {
                        n >= 100 ? (dropzone.emit("success", e), dropzone.emit("complete", e), clearInterval(a)) : (n++, (t.style.width = n + "%"));
                    }, 20);
            });
        }),
            t.querySelector(".dropzone-remove-all").addEventListener("click", function () {
                Swal.fire({
                    text: "Are you sure you would like to remove all files?",
                    icon: "warning",
                    showCancelButton: !0,
                    buttonsStyling: !1,
                    confirmButtonText: "Yes, remove it!",
                    cancelButtonText: "No, return",
                    customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                }).then(function (e) {
                    e.value
                        ? ((t.querySelector(".dropzone-upload").style.display = "none"),
                            (t.querySelector(".dropzone-remove-all").style.display = "none") ,
                            dropzone.removeAllFiles(!0))
                        : "cancel" === e.dismiss &&
                        Swal.fire({ text: "Your files was not removed!.",
                            icon: "error", buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: { confirmButton: "btn btn-primary" } });
                });
            }),
            dropzone.on("queuecomplete", function (e) {
                t.querySelectorAll(".dropzone-upload").forEach((e) => {
                    e.style.display = "none";
                });
            })
        dropzone.on("removedfile", function (e) {
            dropzone.files.length < 1 && ((t.querySelector(".dropzone-upload").style.display = "none") (t.querySelector(".dropzone-remove-all").style.display = "none"));
        });

    </script>
@endpush
