<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contact extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function display_group(): BelongsTo
    {
        return $this->belongsTo(ContactGroup::class);
    }

    public function custom_fields(): HasMany
    {
        return $this->hasMany(ContactsCustomField::class,'contact_id','id')
            ->select('tag','value');
    }

    public function display_name(): string
    {
        return $this->first_name.' '.$this->last_name;
    }
}
