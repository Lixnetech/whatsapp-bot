"use strict";
var KTInboundMessagesList = (function () {
    var e,t, n, r, o = document.getElementById("kt_table_inbound_messages"),
        b = () => {
            o.querySelectorAll('[data-kt-message-table-filter="delete_row"]')
                .forEach((t) => {
                    t.addEventListener("click", function (t) {
                        t.preventDefault();
                        const n = t.target.closest("tr"),
                            r = n.querySelectorAll("td")[1].querySelectorAll("a")[1].innerText;
                        Swal.fire({
                            text: "Are you sure you want to delete " + r + "?",
                            icon: "warning",
                            showCancelButton: !0,
                            buttonsStyling: !1,
                            confirmButtonText: "Yes, delete!",
                            cancelButtonText: "No, cancel",
                            customClass: { confirmButton: "btn fw-bold btn-danger", cancelButton: "btn fw-bold btn-active-light-primary" },
                        }).then(function (t) {
                            t.value
                                ? Swal.fire({ text: "You have deleted " + r + "!.", icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, got it!", customClass: { confirmButton: "btn fw-bold btn-primary" } })
                                    .then(function () {
                                        e.row($(n)).remove().draw();
                                    })
                                    .then(function () {
                                        k();
                                    })
                                : "cancel" === t.dismiss && Swal.fire({ text: customerName + " was not deleted.", icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, got it!", customClass: { confirmButton: "btn fw-bold btn-primary" } });
                        });
                    });
                });
        },
        f = () => {
            const b = o.querySelectorAll('[type="checkbox"]');
            (t = document.querySelector('[data-kt-message-table-toolbar="base"]')),
                (n = document.querySelector('[data-kt-message-table-toolbar="selected"]')),
                (r = document.querySelector('[data-kt-message-table-select="selected_count"]'));
            const s = document.querySelector('[data-kt-message-table-select="delete_selected"]');
            b.forEach((e) => {
                e.addEventListener("click", function () {
                    setTimeout(function () {
                        k();
                    }, 50);
                });
            }),
                s.addEventListener("click", function () {
                    Swal.fire({
                        text: "Are you sure you want to delete selected customers?",
                        icon: "warning",
                        showCancelButton: !0,
                        buttonsStyling: !1,
                        confirmButtonText: "Yes, delete!",
                        cancelButtonText: "No, cancel",
                        customClass: { confirmButton: "btn fw-bold btn-danger", cancelButton: "btn fw-bold btn-active-light-primary" },
                    }).then(function (t) {
                        t.value
                            ? Swal.fire({ text: "You have deleted all selected customers!.", icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, got it!", customClass: { confirmButton: "btn fw-bold btn-primary" } })
                                .then(function () {
                                    b.forEach((t) => {
                                        t.checked &&
                                        e
                                            .row($(t.closest("tbody tr")))
                                            .remove()
                                            .draw();
                                    });
                                    o.querySelectorAll('[type="checkbox"]')[0].checked = !1;
                                })
                                .then(function () {
                                    k(), f();
                                })
                            : "cancel" === t.dismiss &&
                            Swal.fire({
                                text: "Selected customers was not deleted.",
                                icon: "error", buttonsStyling: !1,
                                confirmButtonText: "Ok, got it!",
                                customClass: { confirmButton: "btn fw-bold btn-primary" } });
                    });
                });
        };
    const k = () => {
        const e = o.querySelectorAll('tbody [type="checkbox"]');
        let c = !1,
            l = 0;
        e.forEach((e) => {
            e.checked && ((c = !0), l++);
        }),
            c ? ((r.innerHTML = l), t.classList.add("d-none"),
                n.classList.remove("d-none")) : (t.classList.remove("d-none"),
                n.classList.add("d-none"));
    };
    return {
        init: function () {
            o &&
            (o.querySelectorAll("tbody tr").forEach((e) => {
                const t = e.querySelectorAll("td");
            }),(e = $(o).DataTable({
                info: !1,
                order: [],
                pageLength: 10,
                lengthChange: false,
                columnDefs: [
                    { orderable: !1, targets: 0 },
                ],
            })).on("draw", function () {
                f(), b(), k();
            }),
                f(),
                document.querySelector('[data-kt-message-table-filter="search"]').addEventListener("keyup", function (t) {
                    e.search(t.target.value).draw();
                }),

                b(),
                (() => {
                    const t = document.querySelector('[data-kt-messsage-table-filter="form"]');
                    n.addEventListener("click", function () {
                        var t = "";
                        r.forEach((e, n) => {
                            e.value && "" !== e.value && (0 !== n && (t += " "), (t += e.value));
                        }),
                            e.search(t).draw();
                    });
                })());
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTInboundMessagesList.init();
});
