<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Jobs\ProcessBulkMessage;
use App\Models\Contact;
use App\Models\ContactGroup;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class WhatsAppController extends Controller
{
    public function quickSend(Request $request){
        if($request->isMethod('post')){
            $url = "https://messages-sandbox.nexmo.com/v0.1/messages";
            $params = ["to" => ["type" => "whatsapp", "number" => $request->input('number')],
                "from" => ["type" => "whatsapp", "number" => "14157386170"],
                "message" => [
                    "content" => [
                        "type" => "text",
                        "text" => "Hello from TG3 :) Please reply to this message with a number between 1 and 100"
                    ]
                ]
            ];
            $headers = ["Authorization" => "Basic " . base64_encode(env('NEXMO_API_KEY') . ":" . env('NEXMO_API_SECRET'))];

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $url, ["headers" => $headers, "json" => $params]);
            $data = $response->getBody();
            Log::Info($data);

            return view('thanks');
        }
    }

    public function auth(){
        $url = 'http://api.tg3.lixnet.net/auth/getqr';
        $client = new Client();
        $response = $client->request('GET',$url);
        $data = $response->getBody();
        //echo $data; die;
        if($data == "Authenticated"){
            return $this->chats();
        }
        return view('test',compact('data'));

    }

    public function authCheck(){
        $url = 'http://api.tg3.lixnet.net/auth/check-auth';
        $client = new Client();
        $response = $client->request('GET',$url);
        $auth = $response->getBody();
        //echo $auth; die;
        if($auth == "disconnected"){
            return $this->auth();
        }else{
            return $this->quick_send();
        }
    }

    public function chats(){
        $url = 'http://api.tg3.lixnet.net/contact/getContacts';
        $client = new Client();
        $response = $client->request('GET',$url);
        $resp = $response->getBody();

        $contacts = json_decode($resp);
        foreach ($contacts as $contact){
            $cont = Contact::get()->contains('number',$contact->number);
            if(empty($cont)){
                if(!$contact->isGroup){
                    Contact::Create([
                        'name' => $contact->name,
                        'number' => $contact->number
                    ]);
                }

            }else{
                return $this->quick_send();
            }
        }
        return $this->get_contacts();
    }


    public function quick_send(){
        $groups = ContactGroup::all();
        return view('chats.create',compact('groups'));
    }

    public function send_text(Request $request){
        if($request->isMethod('post')) {
            if ($request->has('group_id') == "all") {
                $numbers = Contact::all();
                if ($numbers != null) {
                    foreach ($numbers as $phone) {
                        ProcessBulkMessage::dispatch($request, $phone)->delay(now()->addMinutes(3));
                    }
                } else
                    return redirect()->back()->with('flash_message', 'No contacts exist on record yet!');

            }elseif ($request->group_id){
                $contacts = Contact::where('group_id',$request->group_id)->get();
                //echo "<pre>"; print_r($contacts);  die();
                foreach ($contacts as $contact){
                    $params = [
                        "message" => $request->get('message')
                    ];
                    $url = 'http://localhost:4000/chat/sendmessage/' . Helper::kenyan($contact);
                    Http::post($url,$params);
                }
            }
            else {
                foreach (explode(',',$request->get('contacts')) as $phone) {
                    $params = [
                        "message" => $request->get('message')
                    ];
                    $url = 'http://localhost:4000/chat/sendmessage/' . Helper::kenyan($phone);
                    Http::post($url,$params);
                }
            }
            return redirect()->back()->with('flash_message', 'Message Sent Successfully');

        }
        $groups = ContactGroup::all();
        return view('chats.create',compact('groups'));
    }
}
