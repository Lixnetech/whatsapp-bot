<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportJobHistory extends Model
{
    use HasFactory;

    const STATUS_PROCESSING = 'processing';
    const STATUS_FINISHED = 'finished';
    const STATUS_FAILED = 'failed';
    const STATUS_CANCELLED = 'cancelled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'type',
        'status',
        'options',
        'import_id',
        'batch_id',
    ];

    /**
     * is processing
     *
     * @return string
     */
    public function isProcessing(): string
    {
        return self::STATUS_PROCESSING;
    }

    /**
     * is finished
     *
     * @return string
     */
    public function isFinished(): string
    {
        return self::STATUS_FINISHED;
    }

    /**
     * is failed
     *
     * @return string
     */
    public function isFailed(): string
    {
        return self::STATUS_FAILED;
    }

    /**
     * is cancelled
     *
     * @return string
     */
    public function isCancelled(): string
    {
        return self::STATUS_CANCELLED;
    }

    /**
     * get single option
     *
     * @param $name
     *
     * @return mixed|string
     */
    public function getOption($name): string
    {
        return $this->getOptions()[$name];
    }

    public function getOptions(): array
    {
        if (empty($this->options)) {
            return [];
        } else {
            return json_decode($this->options, true);
        }
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
