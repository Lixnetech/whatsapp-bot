<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function add_client(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'contact_no' => 'required|unique:users',
            'password' => 'required|min:4|confirmed'
        ]);

        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['username'] = strtolower(trim($request->username));
        $data['contact_no'] = $request->contact_no;
        $data['email'] = strtolower(trim($request->email));
        $data['password'] = bcrypt($request->password);
        $data['is_active'] = $request->is_active;
        $data['role'] = $request->role;

        $user = User::create($data);
        // $user->syncRoles()
        return response()->json(['success' => __('Data Added successfully.')]);
    }
}
