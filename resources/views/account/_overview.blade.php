<!--begin::Card header-->
<div class="card-header cursor-pointer">
    <!--begin::Card title-->
    <div class="card-title m-0">
        <h3 class="fw-bolder m-0">Profile Details</h3>
    </div>
    <!--end::Card title-->
    <!--begin::Action-->
    <a href="#security" data-bs-toggle="tab" class="btn btn-primary align-self-center">Edit Profile</a>
    <!--end::Action-->
</div>
<!--begin::Card header-->
<!--begin::Card body-->
<div class="card-body p-9">
    <!--begin::Row-->
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Full Name</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8">
            <span class="fw-bolder fs-6 text-gray-800">{{$user->customer->first_name}} {{$user->customer->last_name}}</span>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Row-->
    <!--begin::Input group-->
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Company</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 fv-row">
            <span class="fw-bold text-gray-800 fs-6">{{$user->customer->company_name}}</span>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Contact Phone
            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Phone number must be active"></i></label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 d-flex align-items-center">
            <span class="fw-bolder fs-6 text-gray-800 me-2">{{$user->customer->contact_no}}</span>
            <span class="badge badge-success">Verified</span>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Company Site</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8">
            <a href="{{$user->customer->website}}" target="_blank" class="fw-bold fs-6 text-gray-800 text-hover-primary">{{$user->customer->website}}</a>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Country
            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i></label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8">
            <span class="fw-bolder fs-6 text-gray-800">{{$user->customer->country}}</span>
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="row mb-10">
        <!--begin::Label-->
        <label class="col-lg-4 fw-bold text-muted">Allow Changes</label>
        <!--begin::Label-->
        <!--begin::Label-->
        <div class="col-lg-8">
            <span class="fw-bold fs-6 text-gray-800">Yes</span>
        </div>
        <!--begin::Label-->
    </div>
    <!--end::Input group-->
    <!--begin::Notice-->
{{--    <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">--}}
{{--        <!--begin::Icon-->--}}
{{--        <span class="svg-icon svg-icon-2tx svg-icon-warning me-4"><i class="fa fas fa-btc"></i>--}}
{{--        </span>--}}
{{--        <!--end::Icon-->--}}
{{--        <!--begin::Wrapper-->--}}
{{--        <div class="d-flex flex-stack flex-grow-1">--}}
{{--            <!--begin::Content-->--}}
{{--            <div class="fw-bold">--}}
{{--                <h4 class="text-gray-900 fw-bolder">We need your attention!</h4>--}}
{{--                <div class="fs-6 text-gray-700">Your payment was declined. To start using tools, please--}}
{{--                    <a class="fw-bolder" href="#">Add Payment Method</a>.</div>--}}
{{--            </div>--}}
{{--            <!--end::Content-->--}}
{{--        </div>--}}
{{--        <!--end::Wrapper-->--}}
{{--    </div>--}}
    <!--end::Notice-->
</div>
<!--end::Card body-->
