<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Imports\ContactsImport;
use App\Models\ChatMessage;
use App\Models\Contact;
use App\Models\ContactGroup;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ClientController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
        $messages = ChatMessage::where('to','254791979265')->get();

        $sms_outgoing = ChatMessage::currentMonth()
            ->where('from','254791979265')
            ->selectRaw('Day(created_at) as day, count(send_by) as outgoing,send_by')
            ->where('send_by',"to")
            ->groupBy('day')->pluck('day','outgoing')->flip()->sortKeys();

        $sms_inbound = ChatMessage::query()
            ->where(['to'=>'254791979265'])
            ->selectRaw('Day(created_at) as day, count(send_by) as incoming, send_by')
            ->where('send_by',"from")
            ->groupBy('day')->pluck('day','incoming')->flip()->sortKeys();


        $outgoing = (new LarapexChart)->lineChart()
            ->addData('Outgoing',$sms_outgoing->values()->toArray())
            ->setXAxis($sms_outgoing->keys()->toArray());

        $inbound = (new LarapexChart)->lineChart()
            ->addData("inbound", $sms_inbound->values()->toArray())
            ->setXAxis($sms_inbound->keys()->toArray());

        $all_messages = ChatMessage::query()->limit(100)
            ->orderBy('created_at','desc')
            ->get();

        $groups = ContactGroup::where('client_id',Auth::user()->customer->id)->get();

        return view('client.dashboard',compact('contacts','messages','outgoing','inbound',
            'all_messages','groups'));
    }

    public function settings()
    {
        $user = Auth::user();
        return view('account.settings',compact('user'));
    }

    public function update(ProfileRequest $request): RedirectResponse
    {
        //update user records
        $client = Auth::user()->customer;

        $data = $client->update($request->all());

        if($data){
            return redirect()->back()->with('flash_message','Information Updated Successfully');
        }
        return redirect()->back()->with('flash_message','Something went Wrong');
    }

    public function changePassword(Request $request){
        $confirmpwd = Hash::check($request->currentpassword, auth()->user()->password);
//        echo $confirmpwd;
        if($confirmpwd){
            Auth::user()->update([
                'password' => Hash::make($request->newpassword)
            ]);
            Auth::logout();

            $request->session()->invalidate();

            return redirect('/login')->with([
                'status' => 'success',
                'flash_message' => 'Password was Successfully Changed'
            ]);
        }else{
            return response()->json([
                'status' => 'Error',
                'flash_message' => 'Current Password Doesnt match'
            ]);
        }

    }

    public function changeEmail(Request $request): RedirectResponse
    {
        $confirmpwd = Hash::check($request->password, auth()->user()->password);
        if($confirmpwd){
            Auth::user()->update([
                'email' => $request->email
            ]);
        }else{
            return back()->with('flash_message', 'Password Does not match');
        }

        return back()->with('flash_message', 'Email changed Successfully');
    }

    public function avatar()
    {
        $client = Auth::user()->customer;
        if(!empty($client->imagePath())){
            try {
                $image = Image::make($client->imagePath());
                echo "<pre>"; print_r($image); die;
            }catch (NotReadableException $exc){
                $client->avatar = null;
                $client->save();
                $image = Image::make(public_path('assets/media/avatars/blank.png'));
            }
        }else{
            $image = Image::make(public_path('assets/media/avatars/blank.png'));
        }

        return $image->response();

    }

    public function updateAvatar(Request $request): JsonResponse
    {
        $client = Auth::user()->customer;
        $request->validate([
            'avatar' => 'file'
        ]);

            if($request->hasFile('avatar') && $request->file('avatar')->isValid())
            {
                echo "valid";
                $client->removeImage();
                $client->avatar = $client->uploadImage($request->file('avatar'));
                $client->save();

                //TODO: try update whatsapp profile here

                return response()->json([
                    'status' => 'success',
                    'message' => 'Avatar Updated Successfully'
                ]);
            }
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid Image Type'
            ]);

    }

    public function removeAvatar(): JsonResponse
    {
        $client = Auth::user()->customer;
        $client->removeImage();
        $client->avatar = null;
        $client->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Avatar removed successfully'
        ]);
    }
}
