<?php

namespace App\Models;

use App\Helpers\Helper;
use App\Helpers\Tool;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class Campaigns extends Model
{
    use HasFactory;

    /**
     * Campaign status
     */
    const STATUS_NEW = 'new';
    const STATUS_QUEUED = 'queued';
    const STATUS_SENDING = 'sending';
    const STATUS_FAILED = 'failed';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_SCHEDULED = 'scheduled';
    const STATUS_PROCESSING = 'processing';


    /*
     * Campaign type
     */
    const TYPE_ONETIME = 'onetime';
    const TYPE_RECURRING = 'recurring';

    public static $serverPools = [];
    protected $sendingSevers = null;
    protected $senderIds = null;
    protected $currentSubscription;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'run_at', 'delivery_at', 'schedule_time', 'recurring_end'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }


    public function recipients(): HasMany
    {
        return $this->hasMany(CampaignRecipients::class, 'campaign_id');
    }

    public function contactList(): HasMany
    {
        return $this->hasMany(CampaignList::class,'campaign_id');
    }

    public function reports(): HasMany
    {
        return $this->hasMany(Report::class, 'campaign_id', 'id');
    }

    public static function scheduleCycleValues(): array
    {
        return [
            'daily'   => [
                'frequency_amount' => 1,
                'frequency_unit'   => 'day',
            ],
            'monthly' => [
                'frequency_amount' => 1,
                'frequency_unit'   => 'month',
            ],
            'yearly'  => [
                'frequency_amount' => 1,
                'frequency_unit'   => 'year',
            ],
        ];
    }

    public function singleProcess()
    {

        $prepareForTemplateTag = [];
        $contactsData          = [];
        $cutting_array         = [];
        $total_list_contacts   = 0;

        $check_list_count = CampaignList::where('campaign_id', $this->id)->count();
        if ($check_list_count > 0) {

            $list    = CampaignList::where('campaign_id', $this->id)->select('contact_list_id')->cursor();
            $list_id = $list->pluck('contact_list_id')->all();

            Contact::whereIn('group_id', $list_id)->chunk(5000,
                function ($lines) use (&$contactsData) {
                foreach ($lines as $line) {
                    $data = $line->toArray();
                    foreach ($line->custom_fields as $field) {
                        $data[$field->tag] = $field->value;
                    }
                    $contactsData[] = $data;
                }
            });

            $total_list_contacts = count($contactsData);
        }

        if (CampaignRecipients::where('campaign_id', $this->id)->count() > 0) {
            CampaignRecipients::where('campaign_id', $this->id)->select('recipient as phone')
                ->chunk(500, function ($lines) use (&$contactsData, &$total_list_contacts) {
                foreach ($lines as $line) {
                    $data           = $line->toArray();
                    $data['id']     = $total_list_contacts++;
                    $contactsData[] = $data;
                }
            });
        }


        $collection = collect($contactsData);

       // $contact_count  = $this->contactCount($this->cache);


//        $insertData = Tool::check_diff_multi($collection->all(), $cutting_array);
          $insertData = $collection->all();


//        collect($cutting_array)->chunk(1000)->each(function ($lines) use (&$prepareForTemplateTag,  &$total_unit) {
//
//
//            foreach ($lines as $line) {
//
//                $message = $this->renderSMS($this->message, $line);
//
//                $sms_type = $this->sms_type;
//
//                //$sms_count = ceil($sms_count);
//
//                $preparedData['id']             = $line['id'];
//                $preparedData['user_id']        = $this->client_id;
//                $preparedData['phone']          = $line['number'];
//                $preparedData['message']        = $message;
//                $preparedData['sms_type']       = $sms_type;
//                $preparedData['status']         = 'Delivered';
//                $preparedData['campaign_id']    = $this->id;
//
//                $prepareForTemplateTag[] = $preparedData;
//            }
//        });

        collect($insertData)->chunk(5000)->each(function ($lines) use (&$prepareForTemplateTag,
            &$total_unit) {

            foreach ($lines as $line) {
                $message = $this->renderSMS($this->message, $line);

                $sms_type = $this->sms_type;

//                if ($sms_type == 'plain') {
//                    if (strlen($message) != strlen(utf8_decode($message))) {
//                        $sms_type = 'unicode';
//                    }
//
//                    if ($sms_type == 'unicode') {
//                        $length_count = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');
//
//                        if ($length_count <= 70) {
//                            $sms_count = 1;
//                        } else {
//                            $sms_count = $length_count / 67;
//                        }
//                    } else {
//                        $length_count = strlen(preg_replace('/\s+/', ' ', trim($message)));
//                        if ($length_count <= 160) {
//                            $sms_count = 1;
//                        } else {
//                            $sms_count = $length_count / 157;
//                        }
//                    }
//                } elseif ($sms_type == 'unicode') {
//                    $length_count = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');
//
//                    if ($length_count <= 70) {
//                        $sms_count = 1;
//                    } else {
//                        $sms_count = $length_count / 67;
//                    }
//                } else {
//                    $length_count = strlen(preg_replace('/\s+/', ' ', trim($message)));
//                    if ($length_count <= 160) {
//                        $sms_count = 1;
//                    } else {
//                        $sms_count = $length_count / 157;
//                    }
//                }
               // $sms_count = ceil($sms_count);

                $preparedData['id']             = $line['id'];
                $preparedData['user_id']        = $this->client_id;
                $preparedData['phone']          = $line['number'];
                $preparedData['message']        = $message;
                $preparedData['sms_type']       = $sms_type;
                $preparedData['status']         = null;
                $preparedData['campaign_id']    = $this->id;

                $prepareForTemplateTag[] = $preparedData;
            }
        });

//        $user = Client::find($this->client->id);
//        echo "<pre>"; print_r($this->user->id); "</pre>";
//        $user->update([
//            'sms_unit' => $user->sms_unit - $total_unit,
//        ]);

        try {
           // $failed_cost = 0;

            $this->processing();

            collect($prepareForTemplateTag)->sortBy('id')->values()
                ->chunk(3000)->each(function ($sendData) use (&$failed_cost) {
                    foreach ($sendData as $data) {
                          $status = $this->sendWhatsApp($data);
                        if (substr_count($status, 'Delivered') == 1) {
                            $this->updateCache('DeliveredCount');
                        }
                    }
                });

            unset($user);
           // $user = Client::find($this->client->id);
            //echo "<pre>"; print_r($user); "</pre>";
//            $user->update([
//                'sms_unit' => $user->sms_unit + $failed_cost,
//            ]);

            $this->delivered();

        }
        catch (Exception $exception) {
            $this->failed($exception->getMessage());
        } finally {
            self::resetServerPools();
            $this->save();
            //$this->updateCache();
        }

    }

    public static function resetServerPools()
    {
        self::$serverPools = [];
    }


    public function sendWhatsapp($data){
        $phone = $data['phone'];
        $message = $data['message'];

        $params = [
            "message" => $message
        ];
        $url = 'http://localhost:4000/chat/sendmessage/' . Helper::kenyan($phone);
        $resp = Http::post($url,$params);
        if($resp){
            return $resp->status();
        }

        return 'Something Went Wrong';
    }


    public function updateCache($key = null){
        $index = [
            'DeliveredCount' => function($campaign){
                return $campaign->deliveredCount();
            },
            'FailedDeliveredCount' => function ($campaign) {
                return $campaign->failedCount();
            },
            'NotDeliveredCount'    => function ($campaign) {
                return $campaign->notDeliveredCount();
            },
            'ContactCount'         => function ($campaign) {
                return $campaign->contactCount(true);
            },
        ];

        $cache = json_decode($this->cache, true);
        if(is_null($cache)){
            $cache = [];
        }
        if(is_null($key)){
            foreach($index as $key => $callback){
                $cache[$key] = $callback($this);
            }
        }else{
            $callback = $index[$key];
            $cache[$key] = $callback($this);
        }

        $this->cache = json_encode($cache);
        $this->save();
    }

    public function renderSms($msg,$data)
    {
        preg_match_all('~{(.*?)}~s',$msg,$datas);

        foreach ($datas[1] as $value) {
            if (array_key_exists($value, $data)) {
                $msg = preg_replace("/\b$value\b/u", $data[$value], $msg);
            } else {
                $msg = str_ireplace($value, '', $msg);
            }
        }

        return str_ireplace(["{", "}"], '', $msg);
    }

    public static function timeUnitOptions(): array
    {
        return [
            ['value' => 'day', 'text' => 'day'],
            ['value' => 'week', 'text' => 'week'],
            ['value' => 'month', 'text' => 'month'],
            ['value' => 'year', 'text' => 'year'],
        ];
    }

    public function contactCount($cache = false)
    {
        if ($cache) {
            return $this->readCache('ContactCount', 0);
        }
        $list_ids   = $this->contactList()->select('contact_list_id')->cursor()
            ->pluck('contact_list_id')->all();
        $list_count = Contact::whereIn('group_id', $list_ids)->count();

        $recipients_count = $this->recipients()->count();

        return $list_count + $recipients_count;

    }

    public function processing()
    {
        $this->status = self::STATUS_PROCESSING;
        $this->run_at = Carbon::now();
        $this->save();
    }

    public function cancelled()
    {
        $this->status = self::STATUS_CANCELLED;
        $this->save();
    }

    /**
     * Mark the campaign as delivered.
     */
    public function delivered()
    {
        $this->status      = self::STATUS_DELIVERED;
        $this->delivery_at = Carbon::now();
        $this->save();
    }

    public function refreshStatus(): Campaigns
    {
        $campaign     = self::find($this->id);
        $this->status = $campaign->status;
        $this->save();

        return $this;
    }

    public function warning($reason = null)
    {
        $this->reason = $reason;
        $this->save();
    }

    public function failed($reason = null)
    {
        $this->status = self::STATUS_FAILED;
        $this->reason = $reason;
        $this->save();
    }

    public function running()
    {
        $this->status = self::STATUS_PROCESSING;
        $this->run_at = Carbon::now();
        $this->save();
    }

    public function readCache($key, $default = null)
    {
        $cache = json_decode($this->cache, true);
        if (is_null($cache)) {
            return $default;
        }
        if (array_key_exists($key, $cache)) {
            if (is_null($cache[$key])) {
                return $default;
            } else {
                return $cache[$key];
            }
        } else {
            return $default;
        }
    }

    public function preparedDataToSend()
    {

        try {
            // clean up the tracker to prevent the log file from growing very big
           // $this->user->customer->cleanupQuotaTracker();

            // set campaign queued to processing
            $this->running();

            // Reset max_execution_time so that command can run for a long time without being terminated
            Tool::resetMaxExecutionTime();

            $this->singleProcess();

        } catch (Exception $exception) {
            $this->failed($exception->getMessage());
        }

    }

    public function deliveredCount($cache = false): int
    {
        if ($cache) {
            return $this->readCache('DeliveredCount', 0);
        }
        return $this->reports()->where('campaign_id', $this->id)->where('status', 'like', '%Delivered%')->count();
    }


}
