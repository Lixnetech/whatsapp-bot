<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Tool
{
    public static function formatDateTime($dateTime)
    {
        return self::dateTime($dateTime)->format('M dS, Y g:i A');
    }

    public static function dateTime($dateTime)
    {
        $timezone = self::currentTimeZone();
        $result = $dateTime;
        $result = $result->timezone($timezone);

        return $result;
    }

    public static function currentTimeZone(): string
    {
        if(is_object(Auth::user())){
            $timezone = is_object(Auth::user()) ? Auth::user()->timezone : '+00:00';
        }else{
            $timezone = '+00:00';
        }
        return $timezone;
    }

    public static function formatTime($dateTime): string
    {
        return !isset($dateTime) ? "": self::dateTime($dateTime)->format('h:i A');
    }

    public static function formatDate($dateTime): string
    {
        return !isset($dateTime) ? '': self::dateTime($dateTime)->format('M d, Y');
    }

    public static function getFileType($filename)
    {
        $mime_types = [
            'txt'  => 'text/plain',
            'htm'  => 'text/html',
            'html' => 'text/html',
            'php'  => 'text/html',
            'css'  => 'text/css',
            'js'   => 'application/javascript',
            'json' => 'application/json',
            'xml'  => 'application/xml',
            'swf'  => 'application/x-shockwave-flash',
            'flv'  => 'video/x-flv',

            // images
            'png'  => 'image/png',
            'jpe'  => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg'  => 'image/jpeg',
            'gif'  => 'image/gif',
            'bmp'  => 'image/bmp',
            'ico'  => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif'  => 'image/tiff',
            'svg'  => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip'  => 'application/zip',
            'rar'  => 'application/x-rar-compressed',
            'exe'  => 'application/x-msdownload',
            'msi'  => 'application/x-msdownload',
            'cab'  => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3'  => 'audio/mpeg',
            'qt'   => 'video/quicktime',
            'mov'  => 'video/quicktime',

            // adobe
            'pdf'  => 'application/pdf',
            'psd'  => 'image/vnd.adobe.photoshop',
            'ai'   => 'application/postscript',
            'eps'  => 'application/postscript',
            'ps'   => 'application/postscript',

            // ms office
            'doc'  => 'application/msword',
            'rtf'  => 'application/rtf',
            'xls'  => 'application/vnd.ms-excel',
            'ppt'  => 'application/vnd.ms-powerpoint',

            // open office
            'odt'  => 'application/vnd.oasis.opendocument.text',
            'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
        ];

        $arr = explode('.',$filename);
        $ext = strtolower(array_pop($arr));
        if(array_key_exists($ext,$mime_types)){
            return $mime_types[$ext];
        }elseif(function_exists('finfo_open')){
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo,$filename);
            finfo_close($finfo);

            return $mimetype;
        }else{
            return 'application/octet-stream';
        }
    }

    /**
     * Reset max_execution_time so that command
     * can run for a long time without being terminated
     * @return bool
     */
    public static function resetMaxExecutionTime(): bool
    {
        try{
            set_time_limit(0);
            ini_set('max_execution_time',0);
            ini_set('memory_limit','-1');

            return true;
        }catch (Exception $e)
        {
            return false;
        }
    }

    public static function convert($size): string
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2).' '.$unit[$i];
    }

    public static function systemTimeFromString($string, $timezone = null)
    {
        if ($timezone == null) {
            $timezone = self::currentTimezone();
        }

        $time = Carbon::createFromFormat('Y-m-d H:i', $string, $timezone);

        return self::systemTime($time);
    }

    public static function systemTime($time)
    {
        return $time->setTimezone(config('app.timezone'));
    }

    public static function check_diff_multi($array1, $array2)
    {
        foreach (array_chunk($array1,500) as $chunk){
            foreach ($chunk as $key => $value){
                if(in_array($value,$array2)){
                    unset($array1[$key]);
                }
            }
        }

        return $array1;
    }

    public static function allTimeZones(): array
    {
        $zones_array = [];
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone){
            date_default_timezone_set($zone);
            $zones_array[$key]['zone'] = $zone;
            $zones_array[$key]['text'] = '(GMT'.date('P',$timestamp).')'.$zones_array[$key]['zone'];
            $zones_array[$key]['order'] = str_replace('-','1',str_replace('+', '2', date('P', $timestamp))).$zone;
        }
        //sort by offset
        usort($zones_array,function ($a,$b){
            return strcmp($a['order'],$b['order']);
        });

        return $zones_array;
    }

    public static function getTimezoneSelectOptions(): array
    {
        $arr = [];
        foreach (self::allTimeZones() as $timezone) {
            $row   = ['value' => $timezone['zone'], 'text' => $timezone['text']];
            $arr[] = $row;
        }

        return $arr;
    }
}
