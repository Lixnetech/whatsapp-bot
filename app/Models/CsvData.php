<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CsvData extends Model
{
    use HasFactory;

    const TYPE_CONTACT = 'contact';
    const TYPE_CAMPAIGN = 'campaign';

    protected $fillable = [
        'user_id',
        'csv_filename',
        'csv_header',
        'csv_data',
        'ref_id',
        'ref_type'
    ];

}
