<div id="kt_account_profile_details" class="collapse show">
    <!--begin::Form-->
    <form id="" class="form" method="POST" action="{{route('whatsapp.bulk')}}">@csrf
    <!--begin::Card body-->
        <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="row mb-6">
                <label class="col-lg-4 col-form-label fw-bold fs-6">Message</label>
                <div class="col-lg-8 fv-row">
                     <textarea type="text" name="message"  class="form-control  form-control-solid"
                               placeholder="">
                      </textarea>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end py-6 px-9">
            <button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
            <button type="submit" class="btn btn-primary" id="">Send Message</button>
        </div>
    </form>
</div>
