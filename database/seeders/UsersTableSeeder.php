<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'username' => 'admin',
                'email' => 'admin@test.com',
                'password' => bcrypt('admin'),
                'role' => 1,
                'contact_no' => 123454
            ],
            [
                'username' => 'client',
                'email' => 'isuzu@test.com',
                'password' => bcrypt('isuzu1234'),
                'role' => 2,
                'contact_no' => 123454
            ],
        ]);

        Client::insert([
            [
                'first_name' => 'ISUZU',
                'last_name' => 'East Africa',
                'email' => 'isuzu@test.com',
                'contact_no' => 254712345678
            ],
        ]);
    }
}
