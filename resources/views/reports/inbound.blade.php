@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                            <span class="svg-icon svg-icon-1 position-absolute ms-6"><i class="bi bi-search"></i></span>
                            <!--end::Svg Icon-->
                            <input type="text" data-kt-message-table-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Search messages" />
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_inbound_messages">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                            <th class="w-10px pe-2">
                                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input class="form-check-input" type="checkbox" data-kt-check="true"
                                           data-kt-check-target="#kt_table_inbound_messages .form-check-input" value="1" />
                                </div>
                            </th>
                            <th class="min-w-125px">Date</th>
                            <th class="min-w-125px">Direction</th>
                            <th class="min-w-125px">From</th>
                            <th class="min-w-125px">to</th>
                            <th class="min-w-125px">status</th>
                            <th class="text-end min-w-100px">Actions</th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="text-gray-600 fw-bold">

                        @forelse($inbounds as $message)
                            <!--begin::Table row-->
                            <tr>
                                <!--begin::Checkbox-->
                                <td>
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" />
                                    </div>
                                </td>
                                <!--end::Checkbox-->
                                <!--begin::Role=-->
                                <td>{{$message->created_at->diffForHumans()}}</td>
                                <!--end::Role=-->
                                <!--begin::Last login=-->
                                <td>
                                    @if($message->send_by == "from")
                                        <div class="badge badge-success fw-bolder">Inbound</div>
                                    @elseif($message->send_by =="to")
                                        <div class="badge badge-warning fw-bolder">Outbound</div>
                                    @endif
                                </td>
                                <td>
                                    {{App\Helpers\Helper::contactName($message->from)}}
                                </td>
                                <td>
                                    @if(str_replace('@c.us','',$message->to) == '254791979265')
                                        <span style="color: dimgrey; font-weight: bold; font-size: 20px;">TG<span style="color: red; font-weight: bold"> 3</span></span>
                                    @endif
                                </td>
                                <td>
                                    <div class="badge badge-light-success fw-bolder">Delivered</div>
                                </td>

                                <!--begin::Action=-->
                                <td class="text-end">
                                    <div class="d-flex justify-content-end flex-shrink-0">
                                        <a href="javascript:void(0)" id="view-inbound" rel="{{$message->id}}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                            <span class="svg-icon svg-icon-3"><i class="bi bi-eye fs-3"></i> </span>
                                        </a>
                                        <a href="javascript:void(0)" id="del-inbound" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm">
                                            <span class="svg-icon svg-icon-3"><i class="bi bi-trash fs-2"></i></span>
                                        </a>
                                    </div>
                                    <!--end::Menu-->
                                </td>
                                <!--end::Action=-->
                            </tr>
                            <!--end::Table row-->
                        @empty
                            <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600"></tbody>
                        <!--end::Table body-->
                        @endforelse
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#kt_table_inbound_messages').delegate('#view-inbound','click',function (e){
                e.stopPropagation()
                let id = $(this).attr('rel')
                $.ajax({
                    url: "{{url('/reports')}}"+'/' + id + '/view',
                    type: 'POST',
                    data:{
                        _token: "{{csrf_token()}}"
                    },
                    success: function (data) {
                        console.log(data)
                        Swal.fire({
                            html: `<div class="table-responsive">
<table class="table table-striped text-left">

        <tbody>
            <tr>
                <td>{{ "From" }}</td>
                <td>` + data.data.from + `</td>
            </tr>
            <tr>
                <td>{{ "To" }}</td>
                <td>` + data.data.to + `</td>
            </tr>
            <tr>
                <td>{{ "Message" }}</td>
                <td>` + data.data.body + `</td>
            </tr>
            <tr>
                <td>{{ "Type" }}</td>
                <td>` + data.data.media_type + `</td>
            </tr>
            <tr>
                <td>{{ "Status"}}</td>
                <td>` + "Delivered" + `</td>
            </tr>
            <tr>
                <td>{{ "Cost" }}</td>
                <td>` + "Kes. 1" + `</td>
            </tr>

</tbody>
</table>
</div>`})},
                    error: function (reject) {
                        if(reject.status === 422){
                            let errors = reject.responseJSON.errors;
                            $.each(errors,function (key,val) {
                                toastr.warning(value[0],"Attention",{
                                    positionClass: 'toast-top-right',
                                    containerId: 'toast-top-right',
                                    progressBar: true,
                                    closeButton: true,
                                    newestOnTop: true
                                })
                            })
                        }
                    }
                })

            })
        })
    </script>
@endpush
