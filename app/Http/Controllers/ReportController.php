<?php

namespace App\Http\Controllers;

use App\Models\ChatMessage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $messages = ChatMessage::query()->limit(100)
            ->orderBy('created_at','desc')
            ->get();
        return view('reports.all_mesages',compact('messages'));
    }

    public function outbound_msgs()
    {
        $outbounds = ChatMessage::query()->where('to','!=','254791979265')
        ->limit(100)
            ->orderBy('created_at','desc')
            ->get();
        return view('reports.outbound',compact('outbounds'));
    }

    public function inbound_msgs()
    {
        $inbounds = ChatMessage::where('to','254791979265')
            ->limit(100)
            ->orderBy('created_at','desc')
            ->get();
        return view('reports.inbound',compact('inbounds'));
    }

    public function view_message(ChatMessage $id): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'data' => $id
        ]);
    }
}
