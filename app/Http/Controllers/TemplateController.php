<?php

namespace App\Http\Controllers;

use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TemplateController extends Controller
{
    public function index()
    {
        $templates = Template::where('user_id',Auth::user()->id)->get();
        return view('templates.index',compact('templates'));
    }

    public function create(Request $request)
    {
        $template = new Template();
    }
}
