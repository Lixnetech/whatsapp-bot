@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div id="kt_toolbar_container" class="container-fluid  flex-stack">
                <div class="card ">
                    <div class="card-header card-header-stretch">
                        <h3 class="card-title">Group Details</h3>
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#kt_tab_pane_7">Contacts</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#kt_tab_pane_8">Message</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="kt_tab_pane_7" role="tabpanel">
                               @include('contactGroups.grp_contact')
                            </div>

                            <div class="tab-pane fade" id="kt_tab_pane_8" role="tabpanel">
                                @include('contactGroups.grp_message')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
    </div>
    <!--end::Post-->
@endsection
