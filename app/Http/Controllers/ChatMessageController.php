<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\Tool;
use App\Jobs\ProcessBulkMessage;
use App\Jobs\StoreCampaignJob;
use App\Models\CampaignList;
use App\Models\Campaigns;
use App\Models\ChatMessage;
use App\Models\Contact;
use App\Models\ContactGroup;
use App\Models\Template;
use App\Models\TemplateTags;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Throwable;


class ChatMessageController extends Controller
{
    public function save_message(Request $request){
        $data = $request->all();
        Log::info('receipt endpoint hit',$data);
        $msg = $data['msg'];
        $message = new ChatMessage();
        $message->body = $msg['body'];
        $message->from = Helper::formatPhone($msg['from']);
        $message->to = Helper::formatPhone($msg['to']);
        $message->fromMe = $msg['id']['fromMe'];
        $message->msgId = $msg['id']['id'];
        if($msg['hasMedia']){
            $message->media_type = $msg['type'];
            $message->media_url = $msg['mediaKey'];
        }else{
            $message->media_type = $msg['type'];
        }
        if($msg['id']['fromMe']){
            $message->send_by = "to";
        }else{
            $message->send_by = "from";
        }
        $message->save();
    }

    public function fetch_messages()
    {
        $messages = ChatMessage::where('to','254791979265')
            ->groupBy('from')
            ->orderBy('created_at','desc')->get();
        return view('chats.index',compact('messages'));
    }

    public function update_read(Request $request)
    {
        $message = ChatMessage::where('');
    }

    public function media()
    {
        $groups = ContactGroup::all();
        return view('chats.send_file',compact('groups'));
    }

    public function uploadFile(Request $request){
        $file = $request->file('file');
        $filename = $file->getClientOriginalExtension();
        //echo "<pre>"; print_r($filename); die();


        if($request->hasFile('file') && $filename == 'jpeg'){
            $file = $request->file('file');
            $msg = $request->caption;
            $contact = $request->grpId;
            $imageName = base64_encode(file_get_contents($file));

            $numbers = Contact::where('group_id',$contact)->pluck('number');
            if ($numbers != null) {
                foreach ($numbers as $phone) {
                    $params = [
                        "caption" => $msg,
                        "image" => $imageName
                    ];
                    $url = 'http://localhost:4000/chat/sendImage/' . Helper::kenyan($phone);
                    Http::post($url, $params);
                }
            }
        }elseif($request->hasFile('file') && $filename == 'pdf'){
            $file = $request->file('file');
            $msg = $request->caption;
            $contact = $request->grpId;
            $imageName = base64_encode(file_get_contents($file));

            $numbers = Contact::where('group_id',$contact)->pluck('number');
            if ($numbers != null) {
                foreach ($numbers as $phone) {
                    $params = [
                        "caption" => $msg,
                        "image" => $imageName
                    ];
                    $url = 'http://localhost:4000/chat/sendpdf/' . Helper::kenyan($phone);
                    Http::post($url, $params);
                }
            }
        }
    }

    public function sendMediaMessage(Request $request)
    {
        if ($request->group_id == "all") {
            $numbers = Contact::all();
            if ($numbers != null) {
                foreach ($numbers as $phone) {
                    Session::put('media_contacts',$phone);
                }
                return response()->json([
                    'status' => 'success',
                    'data' => ['caption' => $request->caption, 'group_id' => $request->group_id]
                ]);
            } else
                return redirect()->back()->with('flash_message', 'No contacts exist on record yet!');
        }else{
            $numbers = Contact::where('group_id',$request->group_id)->get();
            if ($numbers != null) {
                foreach ($numbers as $phone) {
                    Session::put('media_contacts',$phone);
                }
                return response()->json([
                    'status' => 'success',
                    'data' => ['caption' => $request->caption, 'group_id' => $request->group_id]
                ]);
            } else
                return response()->json(['status' => 'error','msg'=>'Error Occurred']);
        }


    }

    public function sendMessage(Request $request)
    {
        $redis = Redis::connection();
        $data = [
            'message' => $request->message,
            'phone' => $request->phone
        ];
        $redis->publish('message',json_encode($data));
        return response()->json([
            'status' => 'success',
            'message' => 'Success'
        ]);
    }

    public function campaignBuilder()
    {
        $template_tags = TemplateTags::get();
        $groups = ContactGroup::where('client_id',Auth::user()->customer->id)->get();
        $templates = Template::where('client_id',Auth::user()->customer->id)->get();

        return view('campaigns.whatsapp',compact('template_tags','groups','templates'));
    }

    public function storeCampaign(Campaigns $campaign, Request $request): RedirectResponse
    {
        $request->validate([
            'campaign_name'=> 'required',
            'message' => 'required',
            'recipients'       => 'required_if:contact_groups,null|nullable',
            'schedule_date'    => 'required_if:schedule,true|date|nullable',
            'schedule_time'    => 'required_if:schedule,true|date_format:H:i',
            'timezone'         => 'required_if:schedule,true|timezone',
            'frequency_cycle'  => 'required_if:schedule,true',
            'frequency_amount' => 'required_if:frequency_cycle,custom|nullable|numeric',
            'frequency_unit'   => 'required_if:frequency_cycle,custom|nullable|string',
            'recurring_date'   => 'sometimes|date|nullable',
            'recurring_time'   => 'sometimes|date_format:H:i',
        ],
        [
            'recipients.required_if' => 'Please select at least one contact group'
        ]);

        $data = $this->buildCampaign($campaign,$request->except('_token'));

        if(isset($data)){
            if($data->getData()->status = 'success'){
              // echo '<pre>'; print_r($data->getData()); die();
                return redirect()->route('whatsapp.campaign')->with([
                    'flash_message' => $data->getData()->message
                ]);

            }
        }

        return redirect()->route('whatsapp.campaign')->with([
            'flash_message' => 'Looks Like something went Wrong'
        ]);

    }

    public function buildCampaign(Campaigns $campaign, array $request): JsonResponse
    {

        $new_campaign = Campaigns::create([
            'client_id' => Auth::user()->customer->id,
            'campaign_name' => $request['campaign_name'],
            'message' => $request['message'],
            'status' => Campaigns::STATUS_NEW,
        ]);

        if(!$new_campaign){
            return response()->json([
                'status' => 'error',
                'message'=> 'Error Occurred'
            ]);
        }
        $total = 0;
        if(isset($request['contact_groups']) && is_array($request['contact_groups'])
            && count($request['contact_groups']) > 0)
        {
            $contact_groups = ContactGroup::whereIn('id',$request['contact_groups'])
                ->where('client_id', Auth::user()->customer->id)->get();
            foreach ($contact_groups as $group){
                $total += $group->subscribersCount($group->cache);
                CampaignList::create([
                    'campaign_id' => $new_campaign->id,
                    'contact_list_id' => $group->id,
                ]);
            }
        }

        if($total == 0){
            $new_campaign->delete();
            return response()->json([
                'status' => 'error',
                'message' => 'No Contacts Found'
            ]);
        }

        if(isset($request['message'])){
            $length_count = strlen(preg_replace('/\s+/',' ',trim($request['message'])));
            if($length_count <= 160){
                $sms_count = 1;
            }else{
                $sms_count = $length_count / 157;
            }
        }

        //if schedule is available
        if(isset($request['schedule']) && $request['schedule'] == "true")
        {
            $schedule_date = $request['schedule_date'].' '.$request['schedule_time'];
            $schedule_time = Tool::systemTimeFromString($schedule_date, $request['timezone']);
            $new_campaign->timezone = $request['timezone'];
            $new_campaign->status = Campaigns::STATUS_SCHEDULED;
            $new_campaign->schedule_time = $schedule_time;

            if($request['frequency_cycle'] == 'onetime'){
                $new_campaign->schedule_type = Campaigns::TYPE_ONETIME;
            }
            else{
                $recurring_date = $request['recurring_date'].' '.$request['recurring_time'];
                $recurring_end = Tool::systemTimeFromString($recurring_date, $request['timezone']);

                $new_campaign->schedule_type = Campaigns::TYPE_RECURRING;
                $new_campaign->recurring_end = $recurring_end;

                if(isset($request['frequency_cycle'])){
                    if($request['frequency_cycle'] != 'custom'){
                        $schedule_cycle = $campaign::scheduleCycleValues();
                        $limits = $schedule_cycle[$request['frequency_cycle']];
                        $new_campaign->frequency_cycle = $request['frequency_cycle'];
                        $new_campaign->frequency_amount = $request['frequency_amount'];
                        $new_campaign->frequency_unit = $request['frequency_unit'];
                    }else{
                        $new_campaign->frequency_cycle = $request['frequency_cycle'];
                        $new_campaign->frequency_amount = $request['frequency_amount'];
                        $new_campaign->frequency_unit = $request['frequency_unit'];
                    }
                }
            }
        }
        else{
            $new_campaign->status = Campaigns::STATUS_QUEUED;
        }

//        $new_campaign->cache = json_encode([
//            'ContactCount'         => $total,
//            'DeliveredCount'       => 0,
//            'FailedDeliveredCount' => 0,
//            'NotDeliveredCount'    => 0,
//        ]);
//
//        if ($sms_type == 'voice') {
//            $new_campaign->language = $input['language'];
//            $new_campaign->gender   = $input['gender'];
//        }
//
//        if ($sms_type == 'mms') {
//            $new_campaign->media_url = Tool::uploadImage($input['mms_file']);
//        }

        $camp = $new_campaign->save();

        if($camp){
            try{
                if(isset($schedule_time)){
                    $delay_minutes = Carbon::now()->diffInMinutes($schedule_time);
                    StoreCampaignJob::dispatch($new_campaign->id)->delay(now()->addMinutes($delay_minutes));
                }else{
                    StoreCampaignJob::dispatch($new_campaign->id);
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Campaign created Successfully in Background'
                ]);
            }catch (Throwable $exception){
                $new_campaign->delete();

                return  response()->json([
                    'status' => 'error',
                     'message' =>   $exception->getMessage(),
                ]);
            }
        }
        $new_campaign->delete();
        return  response()->json([
            'status' => 'error',
            'message' => 'Something went wrong'
        ]);
    }
}
