<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Intervention\Image\Facades\Image;

/**
 * @method static where(string $string, string $uid)
 * @method static create(array $array)
 * @method static select(string $string, string $string1, string $string2, string $string3, string $string4, string $string5)
 * @method static whereIn(string $string, mixed $ids)
 * @method static cursor()
 * @method static currentMonth()
 * @method static count()
 * @method static offset(mixed $start)
 * @method static whereLike(string[] $array, mixed $search)
 */
class ChatMessage extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function contact(): BelongsTo
    {
        return $this->belongsTo(Contact::class);
    }

    public function scopeCurrentMonth($query)
    {
        return $query->where('created_at','>=',Carbon::now()->firstOfMonth());
    }

    public function getSendBy()
    {
        $sms_type = $this->send_by;

        if ($sms_type == 'from') {
            return '<div class="badge badge-primary text-uppercase mr-1 mb-1"><span>'."outgoing".'</span></div>';
        }

        if ($sms_type == 'to') {
            return '<div class="badge badge-success text-uppercase mr-1 mb-1"><span>'."incoming".'</span></div>';
        }

        return $sms_type;
    }

    public static function uploadImage($file): string
    {
        $path = '/app/marketing/';
        $upload_path = storage_path($path);

        $original_photo_storage = public_path('marketing/original_photos/');
        $large_photos_storage = public_path('marketing/large_photos/');
        $medium_photos_storage = public_path('marketing/medium_photos/');
        $mobile_photos_storage = public_path('marketing/mobile_photos/');
        $tiny_photos_storage = public_path('marketing/tiny_photos/');


        $filename = 'promo-'.time().time().'.'.$file->getClientOriginalName();

        //save to server
        $file->move($original_photo_storage, $filename);

        //create thumbnails
        $img = Image::make($upload_path.$filename);

        $img->resize(860,null,function($c){
            $c->aspectRatio();
        })->save($large_photos_storage.$filename)
        ->resize(640, null,function($c){
            $c->aspectRatio();
        })->save($medium_photos_storage.$filename)
        ->resize(420, null, function ($c){
            $c->aspectRatio();
        })->save($mobile_photos_storage.$filename)
        ->resize(10, null,function ($c){
            $c->aspectRatio();
        })->blur(1)->save($tiny_photos_storage.$filename);

        return $path.$filename;
    }

}
