<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->id();
            $table->string('msgId');
            $table->string('from');
            $table->string('to');
            $table->longText('body');
            $table->longText('media_url')->nullable();
            $table->string('media_type')->nullable();
            $table->string('quotedMsgId')->nullable();
            $table->integer('ack')->default(0);
            $table->boolean('read')->default(false);
            $table->boolean('fromMe')->default(false);
            $table->boolean('isDeleted')->default(false);
            $table->enum('send_by',['from','to'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
