@extends('layouts.index')
@section('content')
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
                     data-bs-target="#kt_account_profile_details" aria-expanded="true"
                     aria-controls="kt_account_profile_details">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Send Messages</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="kt_account_profile_details" class="collapse show">
                    <!--begin::Form-->
                    <form id="" class="form" method="POST" action="{{route('whatsapp.bulk')}}">@csrf
                    <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                        {{--                            <div class="row mb-6">--}}
                        {{--                                <!--begin::Label-->--}}
                        {{--                                <label class="col-lg-4 col-form-label fw-bold fs-6">Send To All</label>--}}
                        {{--                                <!--end::Label-->--}}
                        {{--                                <!--begin::Col-->--}}
                        {{--                                <div class="col-lg-8 fv-row">--}}
                        {{--                                    <input type="checkbox" name="all" value="1">--}}
                        {{--                                </div>--}}
                        {{--                                <!--end::Col-->--}}
                        {{--                            </div>--}}
                        <!--begin::Input group-->
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">
                                    <span class="required">Send By Group</span>
                                    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
                                </label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <select name="group_id" aria-label="Select a Country" data-control="select2" data-placeholder="Select" class="form-select form-select-solid form-select-lg fw-bold">
                                        <option value="">Select a Group...</option>
                                        <option  value="all">All</option>
                                        @foreach($groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Recipients Manual Input</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <span><p>You can copy paste as many numbers as you like or key them in manually,
                                            must be in international format '254xxx' no plus sign</p></span>
                                    <textarea class="form-control form-control-solid" rows="3" placeholder="Enter Phone Numbers separated by commas" name="contacts"></textarea>
                                </div>
                                <!--end::Col-->
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Message</label>
                                <div class="col-lg-8 fv-row">
                                    <textarea type="text" name="message"  class="form-control  form-control-solid" placeholder="Hello from TG3 ,\nTo talk to me, Please reply to this message with a number between 1 and 100\n[reply _*!number your_num*_]\nor Just send [_*hello*_]">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
                            <button type="submit" class="btn btn-primary" id="">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


