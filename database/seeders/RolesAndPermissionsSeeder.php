<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'import contacts']);
        Permission::create(['name' => 'create clients']);
        Permission::create(['name' => 'send messages']);
        Permission::create(['name' => 'create payments']);
        Permission::create(['name' => 'send invoices']);
        Permission::create(['name' => 'create templates']);
        Permission::create(['name' => 'delete clients']);
        Permission::create(['name' => 'general setting']);
        Permission::create(['name' => 'mail setting']);


        // or may be done by chaining
        $role = Role::create(['name' => 'client'])
            ->givePermissionTo(['import contacts', 'send messages','create templates']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
    }
}
