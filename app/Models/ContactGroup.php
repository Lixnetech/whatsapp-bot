<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ContactGroup extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function contacts(): HasMany
    {
        return $this->hasMany(Contact::class,'group_id');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class,'client_id');
    }

    public function updateCache($key = null)
    {
        $index = [
            'SubscribersCount' => function ($group) {
                return $group->subscribersCount();
            },
        ];

        // retrieve cached data
        $cache = json_decode($this->cache, true);
        if (is_null($cache)) {
            $cache = [];
        }

        if (is_null($key)) {
            // update all cache
            foreach ($index as $key => $callback) {
                $cache[$key] = $callback($this);
                if ($key == 'SubscribersCount') {
                    // SubscriberCount cache must always be updated as its value will be used for the others
                    $this->cache = json_encode($cache);
                    $this->save();
                }
            }
        } else {
            // update specific key
            $callback    = $index[$key];
            $cache[$key] = $callback($this);
        }

        // write back to the DB
        $this->cache = json_encode($cache);
        $this->save();

    }

    public function subscribersCount($cache = false): int
    {
        if($cache){
            return $this->readCache('SubscribersCount',0);
        }
        return $this->contacts()->count();
    }

    public function readCache($key, $default = null){
        $cache = json_decode($this->cache,true);
        if(is_null($cache)){
            return $default;
        }
        if(array_key_exists($key,$cache)){
            if(is_null($cache[$key])){
                return $default;
            }else {
                return $cache[$key];
            }
        }else{
            return $default;
        }
    }

    public function getRouteKeyName(): string
    {
        return 'id';
    }
}
