<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Intervention\Image\Facades\Image;

class Client extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function groups(): HasMany
    {
        return $this->hasMany(ContactGroup::class)->orderBy('created_at','desc');
    }

    public function uploadImage($file): string
    {
        $path = '/app/profile/';
        $upload_path = storage_path($path);

        if(!file_exists($upload_path)){
            mkdir($upload_path, 0777, true );
        }
        $filename = 'avatar-'.$this->id.'.'.$file->getClientOriginalExtension();

        //save to server
        $file->move($upload_path, $filename);

        //create thumbnails
        $img = Image::make($upload_path.$filename);

        $img->fit(120,120,function($c){
            $c->aspectRatio();
            $c->upsize();
        })->save($upload_path.$filename.'thumb.jpg');

        return $path.$filename;
    }

    public  function imagePath(): string
    {
        if(!empty($this->avatar) && !empty($this->id)){
            return storage_path($this->avatar).'.thumb.jpg';
        }else{
            return '';
        }
    }

    public function removeImage()
    {
        if(!empty($this->avatar) && ! empty($this->id))
        {
            $path = storage_path($this->avatar);
            if(is_file($path)){
                unlink($path);
            }
            if(is_file($path.'.thumb.jpg')){
                unlink($path.'.thumb.jpg');
            }
        }
    }
}
